<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<script type= "text/javascript"> function showValue(newValue){ document.getElementById("range").innerHTML=newValue ;} </script>
		<title> Commande </title>
	</head>
	
	<body>
		<?php
			require_once 'header.php';
		?>
		<form method="post" id="frmConnection" action="recup.php">
		
			
			<fieldset> 
				<LEGEND align="center">
					Informations de livraison
				</LEGEND>
				Nom : <input type="text" name="nom" required placeholder="Obligatoire"> </br>
				Prénom : <input type="text" name="prenom" required placeholder="Obligatoire" /> </br>
				Adresse de livraison : <input type="text" name="txtAdrLiv" placeholder="Obligatoire" /> </br>
			</fieldset>
			<fieldset>
				<LEGEND align="center">
					Commande
				</LEGEND>
				<select name="choix_produit">
				<datalist name="Produit" id="choix_produit ">
					<option value="Serviette"> Serviette
					<option value="Nappe"> Nappe
					<option value="Setdetable"> Set de table
				</datalist>
				<input type="range" name="Qte" ID="Qte" value="15" max="20" min="0" step="1" onchange="showValue(value)"><span id="range"></span>
				
				</br></br>
				Date à laquelle vous souhaitez recevoir votre commande 
				<input type="date" name="maDate">
				
				</br>
				
				Couleur de votre produit
				<input type="color" name="couleur">
			</fieldset>
			
			
			<input type="submit" name="cmd" id="cmd" value=" Valider " />
			
		</form>
	</body>
</HTML>