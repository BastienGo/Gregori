<?php
$chaine = "J'aime le piano";
if (preg_match("/piano/", $chaine)) {
    echo preg_replace("/piano/", "PIANO" , $chaine);
	
} else {
    echo "Aucun résultat n'a été trouvé.";
}

$chaine2 = "J'adore la programmation";
if (preg_match("/^[aeiouy]/", $chaine2)){
	echo "C'est une voyelle !";
} else if(preg_match("/programmation$/", $chaine2)) {
	echo "Effectivement, c'est bien le dernier mot !";
} else{
	echo "Eh non !";
}

$chaine3 = "youhou :)";
if (preg_match("/:\)/", $chaine3)){
	echo "Oui ! :)";
} else{
	echo "Non ! :(";
}

$chaine4 = "oui@sio.fr";
if (preg_match("/sio.fr$/", $chaine4)){
	echo "C'est le bon domaine !";
} else{
	echo "Ah non, c'est pas bon.";
}

$chaine5 = "Lili est jolie.";
echo preg_replace("/Lili/", "Lulu", $chaine5);
?>