<?php
/**
 * Created by IntelliJ IDEA.
 * User: Grabson
 * Date: 15/03/2019
 * Time: 16:44
 */
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;

$app = new \Slim\App;

$app->get('/zaza', function(Request $request, Response $response){
    return "wazaaaaaaaa";
});

$app->get('/personnage/{name}', function(Request $request, Response $response){
    $name = $request->getAttribute('name');
    return getPersonnage($name);
});
function connexion(){
    return $dbh = new PDO("mysql:host=sl-eu-lon-2-portal.13.dblayer.com:29113;dbname=got", 'admin', 'ZXZCCYSSOSQMROLJ', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}
function getPersonnage($name){
    $sql = "SELECT * FROM characters WHERE name = '".$name."'";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS);
        return json_encode($result, JSON_PRETTY_PRINT);
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}

$app->get('/user', function(Request $request, Response $response){
    $tb = $request->getQueryParams();
    $token = $tb["token"];
    if(validJWT($token)){
        $login = $tb["login"];
        $pw = $tb["pwd"];
        //fonction d'insertion
        return checkUser($login, $pw);
    }
    else{
        return $response->withStatus(401);
    }
});

function checkUser($login, $pw){
    $t = 0;
    $sql = "SELECT * FROM utilisateur WHERE login = '".$login."' AND password = '".$pw."'";
    try{
        $dbh=connexion();
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
    $statement = $dbh->prepare($sql);
    $statement->execute();
    foreach ($statement->fetchAll(PDO::FETCH_OBJ) as $ligne){
        $t = 1;
        return "Connexion réussie.<br>".$ligne->email;
    }
    if ($t == 0) {
        return "Connexion échoué.";
    }
}

$app->post('/user', function(Request $request, Response $response){
    $tb = $request->getQueryParams();
    $token = $tb["token"];
    if(validJWT($token)){
        $login = $tb['login'];
        $mail = $tb['email'];
        $password = $tb['pwd'];
        return insertUser($login, $mail, $password);
    }
    else{
        return $response->withStatus(401);
    }
});


function insertUser($login, $mail, $password){
    $sql = "INSERT INTO utilisateur (login, email, dateInscription, password) VALUES ('".$login."', '".$mail."', '".date("Y,m-d")."', '".$password."');";
    try {
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS);
        return "Inscription réussie !";
    }
    catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}

$app->delete('/user/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');
    return deleteUser($id);
});

function deleteUser($id){
    $sql = "DELETE FROM utilisateur WHERE id = ".$id.";";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS);
        return "Suppression effectuée avec succès.";
    }
    catch(PDOException $e){
        return '{"error"}:'.$e->getMessage().'}';
    }
}

$app->put('/user', function(Request $request, Response $response){
    $tb = $request->getQueryParams();
    $token = $tb["token"];
    if(validJWT($token)){
        $login = $tb['login'];
        $newMail = $tb['email'];
        return modifMail($login,$newMail);
    }
    else{
        return $response->withStatus(401);
    }
});


function modifMail($login,$newMail){
    $sql = "UPDATE utilisateur SET email = '".$newMail."' WHERE login = '".$login."';";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS);
        return "Email mis à jour !";
    }
    catch(PDOException $e){
        return '{Error}:'.$e->getMessage().'}';
    }
}


$app->get('/persos', function(Request $request, Response $response){
    return getPersos();
});

function getPersos(){
    $sql = "SELECT * FROM characters LIMIT 100";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS);
        return json_encode($result, JSON_PRETTY_PRINT);
    }
    catch(PDOException $e){
        return '{Error}:'.$e->getMessage().'}';
    }
}

function checkUserToken($login, $pw){
    $sql = "SELECT * FROM utilisateur WHERE login = '".$login."' AND password = '".$pw."'";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->execute();
        foreach($statement->fetchAll(PDO::FETCH_CLASS) as $ligne){
            return true;
        }
        return false;
    }
    catch(PDOException $e){
        return false;
    }
}

$app->get('/obtentionToken', function(Request $request, Response $response){
    //vérification de l'utilisateur
    $tb = $request->getQueryParams();
    $login = $tb['login'];
    $pwd = $tb['pwd'];
    $allowed= checkUserToken($login,$pwd);
    if($allowed){
        return getTokenJWT();
    }else{
        return $response->withStatus(401);
    }
});

$app->post('/token', function(Request $request, Response $response){
    $tb = $request->getQueryParams();
    $token = $tb['token'];
    if(validJWT($token)){
        return true;
    }else{
        //non autorisé
        return $response->withStatus(401);
    }
});


function getTokenJWT() {
    // Make an array for the JWT Payload
    $payload = array(
        //30 min
        "exp" => time() + (60 * 1)
    );
    // encode the payload using our secretkey and return the token
    return JWT::encode($payload, "secret");
}

function validJWT($token) {
    $res = false;
    try {
        $decoded = JWT::decode($token, "secret", array('HS256'));
    } catch (Exception $e) {
        return $res;
    }
    $res = true;
    return $res;
}



$app->run();
?>
