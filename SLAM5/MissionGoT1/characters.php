<?php

require_once("Culture.php");

abstract class Characters
{
	protected $id;
	protected $nom;
	protected $dateNaiss;
	protected $dateMort;
	protected $culture;
	static $compte = 0;

    /**
     * Characters constructor.
     * @param $id
     * @param $nom
     * @param $dateNaiss
     * @param $dateMort
     * @param $culture
     */
    public function __construct($id, $nom, $dateNaiss, $dateMort, $culture)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->dateNaiss = $dateNaiss;
        $this->dateMort = $dateMort;
        $this->culture = $culture;
        self::$compte++;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getDateNaiss()
    {
        return $this->dateNaiss;
    }

    /**
     * @param mixed $dateNaiss
     */
    public function setDateNaiss($dateNaiss)
    {
        $this->dateNaiss = $dateNaiss;
    }

    /**
     * @return mixed
     */
    public function getDateMort()
    {
        return $this->dateMort;
    }

    /**
     * @param mixed $dateMort
     */
    public function setDateMort($dateMort)
    {
        $this->dateMort = $dateMort;
    }

    /**
     * @return mixed
     */
    public function getCulture()
    {
        return $this->culture;
    }

    /**
     * @param mixed $culture
     */
    public function setCulture($culture)
    {
        $this->culture = $culture;
    }

    public function __toString()
    {
        return $this->nom." est né le ".$this->dateNaiss." et est de culture ".$this->culture.". ";
    }
}
?>