<?php
/**
 * Created by IntelliJ IDEA.
 * User: Grabson
 * Date: 06/09/2018
 * Time: 16:24
 */
require_once("characters.php");
require_once("maison.php");
require_once("hero.php");

$region = new Region(1,"PACA","France");
$maison1 = new Maison(1,"Gregob","Euro","Poule", 1997-06-16, "PACA");
$maison2 = new Maison(2, "Boimak","FrancSuisse","Belette",1999-03-15,"DOMTOM");
$culture1 = new Culture(1,"Goths");
$culture2 = new Culture(2, "Vikings");

$personnage1 = new Hero("Kiliann Boimard",1,"Weazel", "2000-03-15", "Pas mort", "Goths");
$personnage2 = new Hero("Thomas Fernandez", 2, "Shadomazob","1999-06-12", "1999-09-12","Keldorei");
$personnage3 = new Hero("Andy Martin", 3,"Nayd", "1999-11-18", "Pas mort", "Vikings");
$personnage4 = new Hero("Thomas Heron",4, "Wyzard", "1940-08-30","1970-09-20","Aztèque");
$personnage5 = new Hero("Raphael Olivier", 5, "Werulth","1999-09-12", "Pas mort", "Bretons");
$personnage6 = new Hero("Bastien Gregori", 6, "Grabson", "1997-06-16", "Pas mort", "Barbare");

$listeperso = array($personnage1,$personnage2,$personnage3,$personnage4,$personnage5,$personnage6);
foreach($listeperso as $liste){
    echo $liste;
}