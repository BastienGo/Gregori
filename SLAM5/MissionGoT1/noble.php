<?php

class Noble extends Characters
{
	private $myhome;
	private $epouxse;
	private $pere;
	private $mere;

    /**
     * Noble constructor.
     * @param $myhome
     * @param $epouxse
     * @param $pere
     * @param $mere
     */
    public function __construct($myhome, $epouxse, $pere, $mere, $id, $nom, $dateNaiss, $dateMort, $culture)
    {
        parent:: __construct($id,$nom,$dateNaiss,$dateMort,$culture);
        $this->myhome = $myhome;
        $this->epouxse = $epouxse;
        $this->pere = $pere;
        $this->mere = $mere;
    }


    public function getMyHome()
    {
        return $this->myhome;
    }

    /**
     * @param mixed $maison
     */
    public function setMyHome($myhome)
    {
        $this->myhome = $myhome;
    }

    /**
     * @return mixed
     */
    public function getEpouxse()
    {
        return $this->epouxse;
    }

    /**
     * @param mixed $epouxse
     */
    public function setEpouxse($epouxse)
    {
        $this->epouxse = $epouxse;
    }

    /**
     * @return mixed
     */
    public function getPere()
    {
        return $this->pere;
    }

    /**
     * @param mixed $pere
     */
    public function setPere($pere)
    {
        $this->pere = $pere;
    }

    /**
     * @return mixed
     */
    public function getMere()
    {
        return $this->mere;
    }

    /**
     * @param mixed $mere
     */
    public function setMere($mere)
    {
        $this->mere = $mere;
    }


}
?>