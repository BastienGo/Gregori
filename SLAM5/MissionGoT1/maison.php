<?php

require_once("Region.php");
class Maison
{
	private $id;
	private $nom;
	private $devise;
	private $armoiries;
	private $dateFond;
	private $region;

    /**
     * Maison constructor.
     * @param $id
     * @param $nom
     * @param $devise
     * @param $armoiries
     * @param $dateFond
     * @param $region
     */
    public function __construct($id, $nom, $devise, $armoiries, $dateFond, $region)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->devise = $devise;
        $this->armoiries = $armoiries;
        $this->dateFond = $dateFond;
        $this->region = $region;
    }


    /**
 * @return mixed
 */
public function getId()
{
    return $this->id;
}/**
 * @param mixed $id
 */
public function setId($id)
{
    $this->id = $id;
}/**
 * @return mixed
 */
public function getNom()
{
    return $this->nom;
}/**
 * @param mixed $nom
 */
public function setNom($nom)
{
    $this->nom = $nom;
}/**
 * @return mixed
 */
public function getDevise()
{
    return $this->devise;
}/**
 * @param mixed $devise
 */
public function setDevise($devise)
{
    $this->devise = $devise;
}/**
 * @return mixed
 */
public function getArmoiries()
{
    return $this->armoiries;
}/**
 * @param mixed $armoiries
 */
public function setArmoiries($armoiries)
{
    $this->armoiries = $armoiries;
}/**
 * @return mixed
 */
public function getDateFond()
{
    return $this->dateFond;
}/**
 * @param mixed $dateFond
 */
public function setDateFond($dateFond)
{
    $this->dateFond = $dateFond;
}/**
 * @return mixed
 */
public function getRegion()
{
    return $this->region;
}/**
 * @param mixed $region
 */
public function setRegion($region)
{
    $this->region = $region;
}
}
?>