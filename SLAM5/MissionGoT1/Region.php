<?php
/**
 * Created by IntelliJ IDEA.
 * User: Grabson
 * Date: 06/09/2018
 * Time: 16:29
 */

class Region
{
    private $id;
    private $libelle;
    private $pays;

    /**
     * Region constructor.
     * @param $id
     * @param $libelle
     * @param $pays
     */
    public function __construct($id, $libelle, $pays)
    {
        $this->id = $id;
        $this->libelle = $libelle;
        $this->pays = $pays;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * @param mixed $pays
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    }


}