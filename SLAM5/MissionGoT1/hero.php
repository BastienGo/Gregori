<?php

class Hero extends Characters
{
	private $nomActeur;

    /**
     * Hero constructor.
     * @param $nomActeur
     */
    public function __construct($nomActeur, $id, $nom, $dateNaiss, $dateMort, $culture)
    {
        parent:: __construct($id,$nom,$dateNaiss,$dateMort,$culture);
        $this->nomActeur = $nomActeur;
    }


    /**
     * @return mixed
     */
    public function getNomActeur()
    {
        return $this->nomActeur;
    }

    /**
     * @param mixed $nomActeur
     */
    public function setNomActeur($nomActeur)
    {
        $this->nomActeur = $nomActeur;
    }

    public function __toString()
    {
        echo parent::__toString();
        return "L'acteur jouant ce personnage est ".$this->nomActeur.".";
    }

}

?>