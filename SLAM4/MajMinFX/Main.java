package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25,25,25,25));
        Scene scene = new Scene(grid, 300, 250, Color.MIDNIGHTBLUE);

        Text scenetitle = new Text("MajMin");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label textachang = new Label("Texte : ");
        grid.add(textachang, 0, 1);

        TextField text = new TextField();
        grid.add(text, 1, 1);

        final Text actiontarget = new Text();

        Button btn2 = new Button("Maj");
        grid.add(btn2,1,4);

        btn2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                actiontarget.setFill(Color.DARKRED);
                actiontarget.setText(text.getText().toUpperCase());
                text.setText("");
            }
        });

        Button btn = new Button("Min");
        grid.add(btn, 2, 4);


        grid.add(actiontarget,1,6);
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                actiontarget.setFill(Color.MIDNIGHTBLUE);
                actiontarget.setText(text.getText().toLowerCase());
                text.setText("");
            }
        });

        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
