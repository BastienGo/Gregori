package eu.hautil.joueurfoot;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class MainSimple {
    public static void main(String[] args) {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("driver ok");
            Connection conn=
                    DriverManager.getConnection("jdbc:mysql://localhost/SLAM_4?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","");
            System.out.println("connection ok");

            Statement stmt = conn.createStatement();

            String requestin = "INSERT INTO mdao1_joueurfoot VALUES ('12345678','OuiNom','OuiPrenom','Gardien','1','FCOui','1997-06-16')";
            int res = stmt.executeUpdate(requestin);
            System.out.println("nb de modifications réalisées : " + res);

            String requestsel = "SELECT NOM, PRENOM, NUM FROM mdao1_joueurfoot";
            ResultSet resultsel = stmt.executeQuery(requestsel);
            while(resultsel.next()){
                System.out.println("Nom : " + resultsel.getString(1));
                System.out.println("Prénom : " + resultsel.getString(2));
                System.out.println("Numéro : " + resultsel.getString(3));
            }

            resultsel.close();
            stmt.close();

        }catch(Exception e){
            e.printStackTrace();
        }

    }
}