package eu.hautil.joueurfoot;

import java.util.Date;

/** Classe JoueurFoot
 * @author BastienGre
 * @version 1.0
 */
public class JoueurFoot {
    /**
     * lic est une chaine de caractère (ex: 104039DKGJ30)
     */
    private String lic;
    /**
     * nom est une chaine de caractère (ex: Giroud)
     */
    private String nom;
    /**
     * prenom est une chaine de caractère (ex: Olivier)
     */
    private String prenom;
    /**
     * poste est une chaine de caractère (ex : Attaquant gauche)
     */
    private String poste;
    /**
     * num est un entier (ex : 1)
     */
    private int num;
    /**
     * club est une chaine de caractère (ex : Paris Saint Germain)
     */
    private String club;
    /**
     * dateNaissance est une date (ex : 1990-05-10)
     */
    private Date dateNaiss;


    /**
     * Un constructeur à 7 paramètres
     * @param lic
     * @param nom
     * @param prenom
     * @param poste
     * @param num
     * @param club
     * @param dateNaiss
     */
    public JoueurFoot(String lic, String nom, String prenom, String poste, int num, String club, Date dateNaiss) {
        this.lic = lic;
        this.nom = nom;
        this.prenom = prenom;
        this.poste = poste;
        this.num = num;
        this.club = club;
        this.dateNaiss = dateNaiss;
    }

    /**
     * Un constructeur vide
     */
    public JoueurFoot() {

    }

    /**
     * Un constructeur à 6 paramètres
     * @param lic
     * @param nom
     * @param prenom
     * @param num
     * @param poste
     * @param club
     */
    public JoueurFoot(String lic, String nom, String prenom, int num, String poste, String club) {
        this.lic = lic;
        this.nom = nom;
        this.prenom = prenom;
        this.poste = poste;
        this.num = num;
        this.club = club;
    }

    /**
     * Une méthode qui récupère la licence
     * @return lic
     */
    public String getLic() {
        return lic;
    }

    /**
     * Une méthode qui récupère le nom
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Une méthode qui récupère le prenom
     * @return prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Une méthode qui récupère le poste
     * @return poste
     */
    public String getPoste() {
        return poste;
    }

    /**
     * Une méthode qui récupère le numero
     * @return num
     */
    public int getNum(){
        return num;
    }

    /**
     * Une méthode qui récupère le club
     * @return club
     */
    public String getClub(){
        return club;
    }

    /**
     * Une méthode qui récupère la date de naissance
     * @return dateNaiss
     */
    public Date getDatenaiss(){
        return dateNaiss;
    }

    /**
     * Une méthode qui permet de changer la licence
     * @param lic
     */
    public void setLic(String lic) {
        this.lic = lic;
    }

    /**
     * Une méthode qui permet de changer le nom
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Une méthode qui permet de changer le prenom
     * @param prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Une méthode qui permet de changer le poste
     * @param poste
     */
    public void setPoste(String poste) {
        this.poste = poste;
    }

    /**
     * Une méthode qui permet de changer le numero
     * @param num
     */
    public void setNum(int num) {
        this.num = num;
    }

    /**
     * Une méthode qui permet de changer le club
     * @param club
     */
    public void setClub(String club) {
        this.club = club;
    }

    /**
     * Une méthode qui permet de changer la date de naissance
     * @param dateNaiss
     */
    public void setdateNaiss(Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }

    /**
     * Une méthode qui affiche les informations concernant le joueur
     * @return les informations qui concernent le joueur
     */
    @Override
    public String toString() {
        return "Joueur{" +
                "lic = " + lic +
                ", nom = '" + nom +
                ", prenom = " + prenom +
                ", poste = " + poste +
                ", numero = " + num +
                ", club = " + club +
                ", datenaissance = " + dateNaiss + '}';
    }

}