package eu.hautil.joueurfoot;

import java.sql.*;
import java.util.ArrayList;

/** Classe JoueurDAO
 * @author BastienGre
 * @version 1.0
 */
public class JoueurDAO {

    public Connection getConnexion(){
        Connection conn = null;
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost/SLAM_4?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String user= "root";
        String pwd = "";
        try{
            Class.forName(driver);
            System.out.println("driver ok");
            conn=  DriverManager.getConnection(url,user,pwd);
            System.out.println("connection ok");

        }catch(Exception e){
            e.printStackTrace();
        }
        return conn;

    }

    public void insertJoueur(JoueurFoot j) throws SQLException{
        String req1 = "INSERT INTO mdao1_joueurfoot VALUES(?,?,?,?,?,?,?)";
        PreparedStatement pstmt = getConnexion().prepareStatement(req1);

        pstmt.setString(1, j.getLic());
        pstmt.setString(2, j.getNom());
        pstmt.setString(3,j.getPrenom());
        pstmt.setString(4,j.getPoste());
        pstmt.setInt(5,j.getNum());
        pstmt.setString(6,j.getClub());
        pstmt.setDate(7, new Date(j.getDatenaiss().getTime()));

        int res1 = pstmt.executeUpdate();
        System.out.println("Nombre d'insertions réalisées : " + res1);

        getConnexion().close();
    }

    public void deleteJoueur(String licence) throws SQLException{
        String req2 = "DELETE FROM mdao1_joueurfoot WHERE LIC = ?";
        PreparedStatement pstmt2 = getConnexion().prepareStatement(req2);
        pstmt2.setString(1,licence);

        int res2 = pstmt2.executeUpdate();
        System.out.println("Nombre de suppressions réalisées : " + res2);

        getConnexion().close();
    }

    public JoueurFoot getJoueurFootByLic(String lic) throws SQLException{
        JoueurFoot res3 = null;
        String req3 = "SELECT * FROM mdao1_joueurfoot WHERE LIC = ?";
        PreparedStatement pstmt3 = getConnexion().prepareStatement(req3);
        pstmt3.setString(1, lic);

        return res3;

    }

    public ArrayList<JoueurFoot> recupererJoueurs() throws  SQLException{
        ArrayList<JoueurFoot> listejf = new ArrayList<>();
        JoueurFoot res4 = null;
        String req4 = "SELECT * FROM mdao1_joueurfoot";
        PreparedStatement pstmt4 = getConnexion().prepareStatement(req4);
        ResultSet recup = pstmt4.executeQuery();

        while(recup.next()){
            JoueurFoot j = new JoueurFoot();
            j.setLic(recup.getString(1));
            j.setNom(recup.getString(2));
            j.setPrenom(recup.getString(3));
            j.setPoste(recup.getString(4));
            j.setNum(recup.getInt(5));
            j.setClub(recup.getString(6));
            j.setdateNaiss(recup.getDate(7));

            listejf.add(j);

        }
        return listejf;

    }
}
