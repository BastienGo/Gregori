package eu.hautil.joueurfoot;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Date;

public class MainDAO {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {

            System.out.println("1 pour ajouter, 2 pour rechercher un joueur, 3 pour supprimer, 4 pour quitter en affichant la liste de joueurs : ");
            int choix = sc.nextInt();

            while (choix < 1 || choix > 4) {
                System.out.println("Ce chiffre n'est pas attribué, recommencez : ");
                choix = sc.nextInt();
            }

            if (choix == 1) {
                JoueurDAO j = new JoueurDAO();
                System.out.println("Saisissez successivement la licence, le nom, le prénom, le poste, le numéro du joueur, le club du joueur et sa date de naissance : ");

                String lic = sc.next();
                String nom = sc.next();
                String prenom = sc.next();
                String poste = sc.next();
                int num = sc.nextInt();
                String club = sc.next();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String dateNaiss =  sc.next();
                Date oui = null;
                try{
                    oui = sdf.parse(dateNaiss);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                JoueurFoot jf = new JoueurFoot(lic,nom,prenom, poste, num, club, oui);
                j.insertJoueur(jf);

            }

            if (choix == 2) {
                JoueurDAO j = new JoueurDAO();
                System.out.println("Saisissez le numéro de licence du joueur à rechercher : ");

                String lic = sc.next();

                j.getJoueurFootByLic(lic);
            }

            if (choix == 3) {
                JoueurDAO j = new JoueurDAO();
                System.out.println("Saisissez le numéro de licence du joueur à supprimer : ");

                String lic = sc.next();

                j.deleteJoueur(lic);
            }

            if (choix == 4) {
                JoueurDAO jdao = new JoueurDAO();
                ArrayList<JoueurFoot> listejf;

                listejf = jdao.recupererJoueurs();
                for(JoueurFoot j : listejf){
                    System.out.println(j);
                }
            }

        } catch (DAOException e) {
            e.printStackTrace();
        }
    }
}