package eu.hautil.joueurfoot;

import java.sql.*;
import java.util.ArrayList;

/** Classe JoueurDAO
 * @author BastienGre
 * @version 1.0
 */
public class JoueurDAO {

    /**
     * Une méthode qui permet de créer une connexion à la base de donnée
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public Connection getConnexion() throws SQLException, ClassNotFoundException{
        Connection conn = null;
        String driver = "om.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost/SLAM_4?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String user= "root";
        String pwd = "";
            Class.forName(driver);
            System.out.println("driver ok");
            conn=  DriverManager.getConnection(url,user,pwd);
            System.out.println("connection ok");
        return conn;

    }

    /**
     * Une méthode qui permet d'ajouter un joueur dans la BDD
     * @param j
     * @throws DAOException
     */
    public void insertJoueur(JoueurFoot j) throws DAOException{
        try {
            String req1 = "INSERT INTO mdao1_joueurfoot VALUES(?,?,?,?,?,?,?)";
            PreparedStatement pstmt = getConnexion().prepareStatement(req1);

            pstmt.setString(1, j.getLic());
            pstmt.setString(2, j.getNom());
            pstmt.setString(3, j.getPrenom());
            pstmt.setString(4, j.getPoste());
            pstmt.setInt(5, j.getNum());
            pstmt.setString(6, j.getClub());
            pstmt.setDate(7, new Date(j.getDatenaiss().getTime()));

            int res1 = pstmt.executeUpdate();
            System.out.println("Nombre d'insertions réalisées : " + res1);

            getConnexion().close();
        }
        catch(SQLException e){
            DAOException ex = new DAOException("Y'a un problème au niveau de la BDD !", e);
            throw ex;
        }
        catch(ClassNotFoundException cnfe){
            DAOException ex = new DAOException("Y'a un problème au niveau du driver !", cnfe);
            throw ex;
        }
    }

    /**
     * Une méthode qui permet de supprimer un joueur de la BDD
     * @param licence
     * @throws DAOException
     */
    public void deleteJoueur(String licence) throws DAOException{
        try {
            String req2 = "DELETE FROM mdao1_joueurfoot WHERE LIC = ?";
            PreparedStatement pstmt2 = getConnexion().prepareStatement(req2);
            pstmt2.setString(1, licence);

            int res2 = pstmt2.executeUpdate();
            System.out.println("Nombre de suppressions réalisées : " + res2);

            getConnexion().close();
        }
        catch(SQLException e){
            DAOException ex = new DAOException("Y'a un problème au niveau de la BDD !", e);
            throw ex;
        }
        catch(ClassNotFoundException cnfe){
            DAOException ex = new DAOException("Y'a un problème au niveau du driver !", cnfe);
            throw ex;
        }
    }

    public JoueurFoot getJoueurFootByLic(String lic) throws DAOException{
        JoueurFoot res3 = null;
        try {
            String req3 = "SELECT * FROM mdao1_joueurfoot WHERE LIC = ?";
            PreparedStatement pstmt3 = getConnexion().prepareStatement(req3);
            pstmt3.setString(1, lic);
        }
        catch(SQLException e){
            DAOException ex = new DAOException("Y'a un problème au niveau de la BDD !", e);
            throw ex;
        }
        catch(ClassNotFoundException cnfe){
            DAOException ex = new DAOException("Y'a un problème au niveau du driver !", cnfe);
            throw ex;
        }
        return res3;
    }

    public ArrayList<JoueurFoot> recupererJoueurs() throws DAOException{
        ArrayList<JoueurFoot> listejf = new ArrayList<>();
        try {
            JoueurFoot res4 = null;
            String req4 = "SELECT * FROM mdao1_joueurfoot";
            PreparedStatement pstmt4 = getConnexion().prepareStatement(req4);
            ResultSet recup = pstmt4.executeQuery();

            while (recup.next()) {
                JoueurFoot j = new JoueurFoot();
                j.setLic(recup.getString(1));
                j.setNom(recup.getString(2));
                j.setPrenom(recup.getString(3));
                j.setPoste(recup.getString(4));
                j.setNum(recup.getInt(5));
                j.setClub(recup.getString(6));
                j.setdateNaiss(recup.getDate(7));

                listejf.add(j);

            }
        }
        catch(SQLException e){
            DAOException ex = new DAOException("Y'a un problème au niveau de la BDD !", e);
            throw ex;
        }
        catch(ClassNotFoundException cnfe){
            DAOException ex = new DAOException("Y'a un problème au niveau du driver !", cnfe);
            throw ex;
        }

        return listejf;
    }
}
