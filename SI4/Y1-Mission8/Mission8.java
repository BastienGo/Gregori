import java.util.Scanner;
class Mission8 {
    static void phrase(String x, int y){
        System.out.println("Vous êtes "+x+" et vous avez "+y+ " ans.");
    }
    static void toutaffi(String x, int y){
        System.out.println(x + " a " + y + " ans.");
    }
    static void categorie(String z){
        System.out.println("Il/Elle appartient donc à la catégorie " + z);
    }
    static int min(int [] tab){
        int min = tab[0];
        for (int i=0; i<5; i++){
            if (min >= tab[i]){
                min = tab[i];
            }
        }
        return min;
    }

    static int max(int [] tab){
        int max = tab[0];
        for (int i=0; i<5; i++) {
            if (max <= tab[i]) {
                max = tab[i];
            }
        }
        return max;
    }

    static double moy(int [] x){
        double moy= 0;
        for (int i=0; i<5; i++){
            moy = moy + x[i];
        }
        moy = moy/5;

        return moy;
    }
        private static Scanner sc;
        public static void main (String[] args){
            sc = new Scanner(System.in);


            String nom[] = new String[5];
            int age[] = new int[5];
            String categorie[] = new String[5];
            for(int i=0; i<5; i++) {
                System.out.println("Saisissez votre nom.");
                nom[i] = sc.next();
                System.out.println("Saisissez votre âge.");
                age[i] = sc.nextInt();


                Mission8.phrase(nom[i], age[i]);


                if(age[i]<0 && age[i]<2){
                    categorie[i] = "nourisson";
                }
                else if(age[i]>=2 && age[i]<12){
                    categorie[i] = "enfant";
                }
                else if(age[i]>=12 && age[i]<55){
                    categorie[i] = "adulte";
                }
                else{
                    categorie[i] = "senior";
                }
            }
            for (int j=0; j<5; j++){
                Mission8.toutaffi(nom[j],age[j]);
                Mission8.categorie(categorie[j]);
            }

            Mission8.min(age);
            int min = Mission8.min(age);
            Mission8.max(age);
            int max = Mission8.max(age);

            Mission8.moy(age);
            double moy = Mission8.moy(age);

            System.out.println("L'âge minimal est " + min + " et l'âge maximal est " + max + ". La moyenne des âges de la liste est de " + moy + "ans.");
        }

    }
