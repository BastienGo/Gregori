import java.util.Scanner;
public class MissionBonus8 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);

        System.out.println("Saisissez un nombre.");
        int nb = sc.nextInt();
        int sum = 0;
        for (int i=1;i <= nb; i++){
            sum = sum + i;
        }
        System.out.println(sum);
    }
}

/*
    variables : nb, sum, i entiers
    Debut
        Saisir nb
        sum <- 0
        Pour i de 1 à nb compris pas 1
            Faire
                sum <- sum + i
        FinPour
        Afficher sum
    Fin
 */