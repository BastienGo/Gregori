import java.util.Scanner;
public class MissionBonus6 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);


        System.out.println("Saisissez un premier nombre à comparer aux trois autres.");
        int nb1 = sc.nextInt();
        System.out.println("Saisissez un second nombre à comparer aux trois autres.");
        int nb2 = sc.nextInt();
        System.out.println("Saisissez un troisième nombre à comparer aux trois autres.");
        int nb3 = sc.nextInt();
        System.out.println("Saisissez un quatrième nombre à comparer aux trois autres.");
        int nb4 = sc.nextInt();

        if (nb1 > nb2 && nb1 > nb3 && nb1 > nb4) {
            System.out.println(nb1 + " est le nombre le plus grand.");
        } else if (nb2 > nb1 && nb2 > nb3 && nb2 > nb4) {
            System.out.println(nb2 + " est le nombre le plus grand.");
        } else if (nb3 > nb1 && nb3 > nb2 && nb3 > nb4) {
            System.out.println(nb3 + " est le nombre le plus grand.");
        } else {
            System.out.println(nb4 + " est le nombre le plus grand.");
        }
    }
}


    /* variables : nb1, nb2, nb3, nb4 entier

    Début
        Saisir nb1
        Saisir nb2
        Saisir nb3
        Saisir nb4
        Si nb1 > nb2 ET nb1 > nb3 ET nb1 > nb4
            Alors
                Afficher nb1, " est le nombre le plus grand."
        Sinon Si nb2 > nb1 ET nb2 > nb3 ET nb2 > nb4
            Alors
                Afficher nb2, " est le nombre le plus grand."
        Sinon Si nb3 > nb1 ET nb3 > nb2 ET nb3 > nb4
            Alors
                Afficher nb3, " est le nombre le plus grand."
        Sinon Afficher nb4, " est le nombre le plus grand."
        FinSi
    Fin
     */
