import java.util.Scanner;
    public class MissionBonus1 {
        private static Scanner sc;

        public static void main(String[] args) {

            sc = new Scanner(System.in);

            System.out.print("Entrez un nombre.");
            int nombre = sc.nextInt();
            if (nombre % 2 == 0) {
                System.out.print("Le nombre est pair.");
            } else {
                System.out.print("Le nombre est impair");
            }
        }
    }

    /*
        variables : nombre entier
        
     Début
        Saisir nombre
        Si nombre%2 = 0
            Alors afficher "Le nombre est pair."
        Sinon
            Afficher "Le nombre est impair."
        FinSi
     Fin
    */
