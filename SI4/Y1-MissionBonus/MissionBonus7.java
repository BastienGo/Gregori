import java.util.Scanner;
public class MissionBonus7 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);

        System.out.println("Saisissez un nombre.");
        int nb = sc.nextInt();
        System.out.println(nb);
        for (int i=0; i<10; i++){
            nb = nb+1;
            System.out.println(nb);
        }
    }
}

/*
    variables : nb, i entiers
    Debut
        Saisir nb
        Afficher nb
        Pour i de 0 à 10 exclus pas 1
            Faire
                nb <- nb+1
                Afficher nb
        FinPour
    Fin
 */