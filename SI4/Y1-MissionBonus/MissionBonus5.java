import java.util.Scanner;
public class MissionBonus5 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);


        System.out.println("Saisissez l'âge de votre enfant.");
        int age = sc.nextInt();
        while(age<6) {
            System.out.println("Veuillez saisir un âge supérieur à 6 ans.");
            age = sc.nextInt();
        }
        if (age == 6 || age == 7) {
            System.out.println("Votre enfant est un \"Poussin\".");
        }
        else if (age == 8 || age == 9) {
            System.out.println("Votre enfant est une \"Pupille\".");
        }
        else if (age==10 || age==11){
            System.out.println("Votre enfant est un \"Minime\".");
        }
        else{
            System.out.println("Votre enfant est un \"Cadet\".");
        }
    }
}

    /* variable : age entier

        Début
            Saisir age
                Tant que age<6
                    Faire
                        Saisir age
                FinTQ
            Si age=6 ou age=7
                Afficher "Votre enfant est un poussin."
            Sinon Si age=8 ou age=9
                Afficher "Votre enfant est une pupille."
            Sinon Si age=10 ou age=11
                Afficher "Votre enfant est un minime."
            Sinon
                Afficher "Votre enfant est un cadet."
            FinSi
        Fin
     */
