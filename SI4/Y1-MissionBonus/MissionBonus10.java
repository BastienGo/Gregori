import java.util.Scanner;
public class MissionBonus10 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);

        int d=0;
        for (int i=1;i<=50;i++){
            d = i*2;
            System.out.println(d);
        }
    }
}

/* variables: d, i entiers
    Debut
        d <- 0
        Pour i entre 1 et 50 compris pas 1
            Faire
                d <- i*2
                Afficher d
        FinPour
    Fin
 */
