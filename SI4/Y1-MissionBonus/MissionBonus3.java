import java.util.Scanner;
public class MissionBonus3 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);


        System.out.println("Saisissez un premier nombre à comparer aux deux autres.");
        int nb1 = sc.nextInt();
        System.out.println("Saisissez un second nombre à comparer aux deux autres.");
        int nb2 = sc.nextInt();
        System.out.println("Saisisser un troisième nombre à comparer aux deux autres.");
        int nb3 = sc.nextInt();

        if (nb1 < nb2) {
            if (nb1 < nb3) {
                System.out.println(nb1 + " est le plus petit des trois nombres.");
            } else if (nb3 < nb1) {
                System.out.println(nb3 + " est le plus petit des trois nombres.");
            }
        } else {
            System.out.println(nb2 + " est le plus petit des trois nombres.");
        }
    }
}


    /* variables : nb1, nb2, nb3 entier

    Début
        Saisir nb1
        Saisir nb2
        Saisir nb3
        
        Si nb1 < nb2
            Alors
                Si nb1 < nb3
                    Alors Afficher nb1, " est le plus petit des trois nombres"
                Sinon si nb3 < nb2
                    Alors Afficher nb3, " est le plus petit des trois nombres"
                FinSi
        Sinon Afficher nb2, " est le plus petit des trois nombres."
        FinSi
    Fin
     */
