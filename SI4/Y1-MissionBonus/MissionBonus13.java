import java.util.Scanner;
public class MissionBonus13 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);

        System.out.println("Saisissez un nombre entier :");
        double n=sc.nextInt();
        double f=n;
        while (f>1){
            f = f-1;
            n = f * n;
        }
        System.out.println(n);
    }
}

/* variables : n, f reels
   Debut
    Saisir n
    f <- n
    Tant que f>1
        Faire
            f <- n-1
            n <- f * n
    FinTQ
    Afficher n
   Fin
 */