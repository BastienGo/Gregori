import java.util.Scanner;
public class MissionBonus12 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);

        System.out.println("Saisissez un nombre entier :");
        int n=sc.nextInt();
        for (int i=0; i<=n;){
            System.out.println(n);
            n = n-1;
        }
    }
}

/* variables : n,i entiers
    Debut
        Saisir n
        Pour i de 0 a n inclus
            Faire
                Afficher n
                n <- n-1
        FinPour
    Fin
 */