import java.util.Scanner;
public class MissionBonus9 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);
        int maxnb=0;

        for (int i=0; i < 5; i++){
            System.out.println("Saisissez un nombre :");
            int nb=sc.nextInt();
            if(nb > maxnb){
                maxnb = nb;
            }
        }
        System.out.println(maxnb);
    }
}

/* variables : maxnb, i, nb entiers
    Debut
        maxnb <- 0
        Pour i de 0 à 5 exclus pas 1
            Faire
                Saisir nb
                Si nb > maxnb
                    maxnb <- nb
                FinSi
        FinPour
        Afficher maxnb
 */