import java.util.Scanner;
public class MissionBonus11 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);

        System.out.println("Saisissez OUI ou NON :");
        String on=sc.next();
        while (on.equals("NON")){
            System.out.println("FAUX! Saisissez OUI pour sortir ou NON pour rester :");
            on = sc.next();
        }
    }
}

/* variable : on chaine de caractere
    Debut
        Saisir on
        Tant que on = NON
            Faire
                Saisir on
        FinTQ
    Fin
 */