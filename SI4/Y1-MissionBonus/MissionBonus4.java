import java.util.Scanner;
public class MissionBonus4 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);


        System.out.println("Saisissez un chiffre entre 1 et 3.");
        int nb = sc.nextInt();
        while (nb > 3 || nb < 1) {
            System.out.println("Le chiffre saisi était incorrect, saisissez-en un autre.");
            nb = sc.nextInt();
        }
    }
}

    /* variable : nb entier
    
        Début
            Saisir nb
            Tant que nb > 3 ou nb < 1
                Faire
                    Saisir nb
            FinTQ
         Fin
     */
