import java.util.Scanner;
public class MissionBonus2 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);

        System.out.println("Saisissez un premier nombre.");
        int nombre1=sc.nextInt();
        System.out.println("Saisissez un second nombre à multiplier au premier.");
        int nombre2=sc.nextInt();
        if(nombre1<0 && nombre2>0 || nombre1>0 && nombre2<0)
        {
        System.out.println("Le produit sera négatif.");
        }
        else
        {
            System.out.println("Le produit sera positif.");
        }
        }
    }
    
    /*
        variable : nombre1, nombre2 entier
        
        Début
            Saisir nombre1
            Saisir nombre2
                Si nombre1>0 et nombre2<0 ou nombre1<0 et nombre2>0
                    Afficher "Le produit sera négatif."
                Sinon
                    Afficher "Le produit sera positif."
                FinSi
        Fin
     */
