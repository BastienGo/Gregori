public class Mission7_4 {
    public static void main(String[] args) {
        String[] nom = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};
        int[][] noteetudiants = {{5, 10, 15}, {4, 9, 14}, {3, 8, 13}, {6, 11, 16}, {7, 12, 17}, {10, 15, 20}, {11, 16, 19}, {9, 4, 3}, {10, 11, 12}, {5, 11, 18}};
        float[] moy = new float[10];
        for(int i=0;i<nom.length;i++){
            moy[i] = (noteetudiants[i][0] + noteetudiants[i][1] + noteetudiants[i][2])/3;
        }
        for(int i = 0;i<10;i++){
            System.out.println(nom[i] + "\n" + moy[i]);
        }
        System.out.println("Et dans l'ordre : ");
        for(int i = 0 ; i<moy.length-1; i++){
            int m = i;
            for(int j=i+1;j<moy.length;j++){
                if(moy[j]<moy[m]){
                    float temp = moy[j];
                    moy[j] = moy[i];
                    moy[i] = temp;
                    String tempo = nom[j];
                    nom[j] = nom[i];
                    nom[i] = tempo;

                }
            }
        }
        for(int i = 0;i<10;i++){
            System.out.println(nom[i]);
            System.out.println(moy[i]);
        }

    }
}

/* variables : i, m, j, temp entiers
                tempo chaine de caracteres
nom tableau de chaine de caracteres initialise
noteetudiants tableau d'entiers initialise
moy tableau de 10 réels

    Debut
        Pour i de 0 à 10 exclus pas 1
            Faire
                moy[i] <- (noteetudiants[i][0] + noteetudiants[i][1] + noteetudiants[i][2])/3
        FinPour
        Pour i de 0 à 10 exclus pas 1
            Faire
                Afficher nom[i], moy[i]
        FinPour
        Afficher "Et dans l'ordre :"
        Pour i de 0 a 9 exclus pas 1
            Faire
                m <- i
                Pour j de i+1 à 10 exclus pas 1
                    Faire
                        Si moy[j] < moy[m]
                            temp <- moy[j]
                            moy[j] <- moy[i]
                            moy[i] <- temp
                            tempo <- nom[j]
                            nom[j] <- nom[i]
                            nom[i] <- tempo
                        FinSi
                FinPour
        FinPour
        Pour i de 0 a 10 exclus pas 1
            Faire
                Afficher nom[i]
                Afficher moy[i]
        FinPour
    Fin
 */