import java.util.Scanner;
public class Mission7_3 {
    private static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);

        String nom[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};
        int noteetudiants[][] = {{5, 10, 15}, {4, 9, 14}, {3, 8, 13}, {6, 11, 16}, {7, 12, 17}, {10, 15, 20}, {11, 16, 19}, {9, 4, 3}, {10, 11, 12}, {5, 11, 18}};
        System.out.println("Quel étudiant recherchez-vous ?");
        String rech=sc.next();
        for (int i=0; i < 10; i++){
            if (rech.equals(nom[i])){
                System.out.println("Les notes de " + nom[i]);
                for (int j=0; j < 3; j++){
                    System.out.println(noteetudiants[i][j]);
                }
                break;
            }
        }
    }
}

/* variables : i,j entiers
    nom tableau de chaines de caracteres de taille 10 initialisé
    noteetudiants tableau d'entiers de taille 30 initialisé

    Debut
        Saisir rech
        Pour i de 0 à 10 exclus pas 1
            Faire
                Si rech = nom[i]
                    Afficher "Les notes de ", nom[i]
                    Pour j entre 0 et 3 exclus pas 1
                        Faire
                            Afficher noteetudiants[i][j]
                    FinPour
                FinSi
        FinPour
    Fin
 */