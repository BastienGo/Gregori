import java.util.Scanner;
public class Mission7_1_2 {
    private static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);

        String nom[] ={"A","B","C","D","E","F","G","H","I","J"};
        int noteetudiants[][]={ {5,10,15},{4,9,14},{3,8,13},{6,11,16},{7,12,17},{10,15,20},{11,16,19},{9,4,3},{10,11,12},{5,11,18}};
        double moy[]= new double[10];
        double m;
        double mc = 0;
        for (int i=0; i < 10; i++){
            m=0;
            System.out.println(nom[i]);
            for(int j=0; j < 3; j++){
                System.out.println(noteetudiants[i][j]);
                m = m + noteetudiants[i][j];
            }
            m = m/3;
            moy[i]=m
            System.out.printf("La moyenne des notes de cet étudiant est %.2f \n",m);
            mc = mc + m;
        }
        mc = mc/10;
        System.out.printf("La moyenne de la classe est %.2f \n", mc);
    }
}

/*
Variables : m, mc double
            moy tableau de réels de taille 10
            nom tableau de chaine de caractères de taille 10 initialisé
            noteetudiants tableau à deux dimensions d'entiers de taille 30 initialisé

Debut
    m <- 0
    mc <- 0
    Pour i allant de 0 à 10 exclus pas 1
        Faire
            m <- 0
            Afficher nom[i]
            Pour j allant de 0 à 3 exclus pas 1
                Faire
                    Afficher noteetudiants[i][j]
                    m <- m + noteetudiants[i][j]
            FinPour
            m <- m/3;
            moy[i] <- m
            Afficher "La moyenne des notes de cet étudiant est ", m avec 2 décimales
            mc <- mc + m
    FinPour
    mc <- mc/10
    Afficher "La moyenne de la classe est ", mc avec 2 décimales
Fin