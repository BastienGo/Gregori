import java.util.Scanner;
public class Mission2 {
    private static Scanner sc;
    public static void main( String[] args ) {

        sc = new Scanner(System.in);

		/*
			variables : HT, TVA, TTC réel
						QAch entier
						prod string
                        necessite booleen

			Début:
			    saisir prod
				saisir QAch
				saisir HT
                saisir necessite

				Si necessite = vrai
				    alors TVA <= 0.05
				Sinon
				    TVA <= 0.2
				FinSi

				TTC <= HT*(1+TVA)*QAch

				Si QAch > 100
					alors
						TTC <= TTC*0.9
				FinSi

				Si QAch = 1
				    Alors Afficher "Le prix de votre ", prod, " sera de ", TTC, "€."
				Sinon
				    Afficher "Le prix de vos ", prod, "sera de ", TTC, "€."
				FinSi

			Fin
		*/

        System.out.println("Veuillez saisir le nom du produit :");
        String prod = sc.nextLine();
        System.out.println("Veuillez préciser la quantité");
        int QAch= sc.nextInt();
        System.out.println("Veuillez saisir le prix Hors Taxe");
        double HT= sc.nextDouble();
        System.out.println("Est-ce un produit de première necessité ? True pour Oui, False pour Non.");
        boolean necessite= sc.nextBoolean();
        double TVA;

        if(necessite=true) {
            TVA = 0.05;
        }
        else{
            TVA = 0.2;
        }

        double TTC= ((HT * (1+ TVA)*QAch));

        if(QAch>100){
            TTC= TTC*0.9;
        }
        if(QAch==1) {
            System.out.println("Le prix de votre " + prod +" sera de " + TTC + "€.");
        }
        else {
            System.out.println("Le prix de vos " + prod + " sera de " + TTC + "€.");
        }
    }
}