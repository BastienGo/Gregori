import java.util.Scanner;
public class Mission5_3 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);

        int rand = 0 + (int)(Math.random() * ((100 - 0) + 1));
        System.out.println("Un nombre entre 0 et 100 à été généré.");

        for(int indice=1; indice<=12; indice++){
            System.out.println("Essayez de deviner le nombre généré !");
            int essai=sc.nextInt();
            if(essai == rand){
                System.out.println("Vous avez trouvé la bonne réponse, bravo !");
                break;
            }
            else{
                System.out.println("Dommage c'est la mauvaise réponse.");
                if(essai > rand){
                    System.out.println("Le nombre à deviner est plus petit.");
                }
                else{
                    System.out.println("Le nombre à deviner est plus grand.");
                }
            }
        }
    }
}


/*
    variables : rand, essai, indice entier

    Début
        Générer entre 0 et 100
        Afficher "Un nombre entre 0 et 100 à été généré."

        Pour indice de 1 à 12 inclus pas 1
            Faire
                Saisir essai
                    Si essai = rand
                        Alors
                            Afficher "Vous avez trouvé la bonne réponse, bravo !"
                            FinPour
                    Sinon
                        Afficher "Dommage c'est la mauvaise réponse."

                        Si essai > rand
                            Alors
                                Afficher "Le nombre à deviner est plus petit."
                        Sinon
                            Afficher "Le nombre à deviner est plus grand."
                        FinSi
                    FinSi
        FinPour
    Fin
 */
