import java.util.Scanner;
public class Mission5_1 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);
        int nb;
        String add= "";
        int somme = 0;
        for(int indice=1 ; indice<=10 ; indice++){
            System.out.println("Saisissez un nombre entier.");
            nb = sc.nextInt();
            add =add + " + " + nb;
            somme = somme + nb;
        }
        System.out.println("0" + add + " = " + somme);
    }
}

/*
    variables : nb, somme, indice entier
                add chaine
    Début :
        somme = 0
        Pour indice de 0 à 10 inclus pas 1
            Faire
                Saisir nb
                add = add, " + ", nb
                somme = somme + nb
        FinPour
        Afficher "0", add, " = ", somme
    Fin
 */
