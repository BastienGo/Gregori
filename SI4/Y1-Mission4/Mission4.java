import java.util.Scanner;
public class Mission4 {
    private static Scanner sc;

    public static void main(String[] args) {

        sc = new Scanner(System.in);

        /*
            variable :  age, choix, achat entier
                        categorie chaine de caractères
                        prixBillet, prixTotal float
            Début :
                    Saisir achat
                        Tant que achat=1
                            Faire
                            Saisir choix
                                Tant que choix>4 ou choix<1
                                Saisir choix
                            FinTQ
                            prixBillet <= 0
                            Si choix = 1
                                alors prixBillet <= 20
                            Sinon Si choix = 2
                                alors prixBillet <= 30
                            Sinon Si choix = 3
                                alors prixBillet <= 35
                            Sinon Si choix = 4
                                alors prixBillet <= 40
                            Sinon
                                afficher "Ce spectacle n'existe pas."
                                Fin
                            FinSi

                        Saisir age
                            Tant que age < 0
                                Faire
                                Saisir age
                            FinTQ
                            Si age < 3
                                alors afficher "Nourisson"
                                categorie = "Nourisson"
                            Sinon Si age >= 3 ET age < 12
                                alors afficher "Enfant"
                                categorie = "Enfant"
                            Sinon Si age >= 12 ET age < 18
                                alors afficher "Adolescent"
                                categorie = "Adolescent"
                            Sinon Si age >= 18 ET age < 55
                                alors afficher "Adulte"
                                categorie = "Adulte"
                            Sinon
                                afficher "Senior"
                                categorie = "Senior"
                            FinSi

                            Si categorie = "Nourisson"
                                prixBillet = 5
                            Sinon Si categorie = "Enfant"
                                prixBillet = prixBillet * 30/100
                            Sinon Si categorie = "Senior"
                                prixBillet = prixBillet * 50/100
                            Sinon
                                prixBillet = prixBillet
                            FinSi
                            Afficher "Le prix du billet est de ", prixBillet, "euros."
                            Saisir achat
                            prixTotal = prixTotal + prixBillet
                        FinTQ
                        Afficher "Le prix total est de ", prixTotal, "euros."
                Fin
         */
        System.out.println("Voulez-vous acheter, ou quitter ? (Entrer 1 pour acheter, 2 pour quitter).");
        int achat = sc.nextInt();
        double prixTotal= 0;
        while (achat==1)
        {

            System.out.println("Veuillez saisir le numéro du spectacle (1, 2, 3 ou 4).");
            int choix = sc.nextInt();
            while (choix > 4 || choix < 1) {
                System.out.println("Veuillez saisir un numéro entre 1 et 4.");
                choix = sc.nextInt();
            }
            float prixBillet = 0;
            if (choix == 1) {
                prixBillet = 20;
            } else if (choix == 2) {
                prixBillet = 35;
            } else if (choix == 3) {
                prixBillet = 40;
            } else if (choix == 4) {
                prixBillet = 50;
            } else {
                System.out.println("Ce spectacle n'existe pas.");
                System.exit(-1);
            }


            System.out.println("Veuillez saisir votre âge");
            int age = sc.nextInt();
            while (age < 0) {
                System.out.println("Veuillez saisir votre âge");
                age = sc.nextInt();
            }
            String categorie;
            if (age < 3) {
                System.out.println("Nourisson");
                categorie = "Nourisson";
            } else if (age >= 3 && age < 12) {
                System.out.println("Enfant");
                categorie = "Enfant";
            } else if (age >= 12 && age < 18) {
                System.out.println("Adolescent");
                categorie = "Adolescent";
            } else if (age >= 18 && age < 55) {
                System.out.println("Adulte");
                categorie = "Adulte";
            } else {
                System.out.println("Senior");
                categorie = "Senior";
            }
            if (categorie.equals("Nourisson")) {
                prixBillet = 5;
            } else if (categorie.equals("Enfant")) {
                prixBillet = prixBillet * 30 / 100;
            } else if (categorie.equals("Senior")) {
                prixBillet = prixBillet * 50 / 100;
            }
            System.out.println("Le prix du billet est de " + prixBillet + "euros.");
            System.out.println("Voulez-vous continuer vos achats ? (1 pour Oui, 2 pour Non).");
            achat=sc.nextInt();
            prixTotal = prixTotal + prixBillet;
        }
        System.out.println("Le prix total est de " + prixTotal + "euros.");
    }
}
