package sample.DAO;

import sample.Tables.Competition;
import sample.Tables.Team;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class InscriptionDAO extends DAO{
    public void addTeamToTurn(Team t, Competition comp) throws SQLException {
        String req1 = "INSERT INTO inscription  VALUES(?,?)";
        PreparedStatement pstmt = getConnection().prepareStatement(req1);

        pstmt.setInt(1,t.getId());
        pstmt.setInt(2,comp.getId());
        ResultSet res1 = pstmt.executeQuery();
        System.out.println(res1);
    }

    public void delTeamFromTurn(Team t, Competition comp) throws SQLException{
        String req2 = "DELETE * FROM inscription WHERE idTeam = ? AND idComp = ?";
        PreparedStatement pstmt = getConnection().prepareStatement(req2);

        pstmt.setInt(1,t.getId());
        pstmt.setInt(1,comp.getId());
        ResultSet res2 = pstmt.executeQuery();
        System.out.print(res2);
    }
}
