package sample.DAO;

import sample.Tables.Competition;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CompetitionDAO extends DAO {
    public void addComp(Competition c) throws SQLException {
        String req1 = "INSERT INTO competition VALUES (?,?,?,?)";
        PreparedStatement pstmt = getConnection().prepareStatement(req1);

        pstmt.setInt(1,c.getId());
        pstmt.setString(2,c.getLibelle());
        pstmt.setDate(3, (Date) c.getDateDebut());
        pstmt.setDate(4, (Date) c.getDateFin());
        int res1 = pstmt.executeUpdate();
        System.out.println("Compétition ajoutée");
    }

    public void delComp(Competition c) throws SQLException {
        String req2 = "DELETE FROM competition WHERE id = ?";
        PreparedStatement supr = getConnection().prepareStatement(req2);
        supr.setInt(1,c.getId());
        int res2= supr.executeUpdate();
        System.out.println("Compétition supprimée");

    }
}
