package sample.DAO;

import sample.Tables.Champions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ChampionsDAO extends DAO {

    public Champions getChampionByNick(String x) {
        Champions res = null;
        try {
            Connection conn = super.getConnection();
            String req = "SELECT * FROM champion WHERE ,name = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, x);
            ResultSet result = pstmt.executeQuery();
            while (result.next()) {
                res = new Champions();
                res.setId(result.getInt(1));
                res.getNom(result.getString(2));
            }
            result.close();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public Champions getChampionById(String x) {
        Champions res = null;
        try {
            Connection conn = super.getConnection();
            String req = "SELECT * FROM champion WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, x);
            ResultSet result = pstmt.executeQuery();
            while (result.next()) {
                res = new Champions();
                res.setId(result.getInt(1));
                res.setNom(result.getString(2));
            }
            result.close();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
