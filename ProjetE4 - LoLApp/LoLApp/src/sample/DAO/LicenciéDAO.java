package sample.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class LicenciéDAO extends DAO {

    public boolean updateNom(String nom, String lic){
        Connection conn;
        boolean r = false;
        try{
            conn = super.getConnection();
            String req = "UPDATE licencie SET nom = ? WHERE lic = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,nom);
            pstmt.setString(2,lic);
            r = pstmt.executeUpdate() == 1;
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return r;
    }

    }
