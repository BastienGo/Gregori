package sample.DAO;

import sample.Tables.Team;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TeamDAO extends DAO {

    public void insertTeam(Team t) {
        try {
            String req1 = "INSERT INTO team (nom, tag, idCoach, idRegion, pays, categorie)VALUES (?,?,?,?,?,?)";
            PreparedStatement pstmt = getConnection().prepareStatement(req1);

            pstmt.setString(1, t.getNom());
            pstmt.setString(2, t.getTag());
            pstmt.setInt(3, t.getIdCoach());
            pstmt.setInt(4, t.getIdRegion());
            pstmt.setString(5, t.getPays());
            pstmt.setBoolean(6, t.getCategorie());
            int res2 = pstmt.executeUpdate();
            System.out.println("nb de modif réalisées : " + res2);
            pstmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void deleteTeam(String id){
        try {
            String req5 = "DELETE FROM team WHERE id = ?";
            PreparedStatement supr = getConnection().prepareStatement(req5);
            supr.setString(1, id);
            int res3 = supr.executeUpdate();
            System.out.println("supr : " + res3);
            supr.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Team getTeamByTag(String tag){
        Team t = new Team();
        Connection conn;
        try {
            conn = super.getConnection();
            String req = "SELECT * FROM team WHERE tag = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,tag);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                t.setId(result.getInt(1));
                t.setTag(result.getString(2));
                t.setNom(result.getString(3));
                t.setIdCoach(result.getInt(4));
                t.setIdRegion(result.getInt(5));
                t.setPays(result.getString(6));
                t.setCategorie(result.getBoolean(7));
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return t;
    }

    public Team getTeamByName(String name){
        Team t = new Team();
        Connection conn;
        try {
            conn = super.getConnection();
            String req = "SELECT * FROM team WHERE nom = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,name);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                t.setTag(result.getString(2));
                t.setId(result.getInt(1));
                t.setNom(result.getString(3));
                t.setIdCoach(result.getInt(4));
                t.setIdRegion(result.getInt(5));
                t.setPays(result.getString(6));
                t.setCategorie(result.getBoolean(7));
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return t;
    }

    public Team getTeamByCoachId(int idCoach){
        Team t = new Team();
        Connection conn;
        try {
            conn = super.getConnection();
            String req = "SELECT * FROM team WHERE idCoach = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setInt(1,idCoach);
            ResultSet result = pstmt.executeQuery();
            while (result.next()){
                t.setTag(result.getString(2));
                t.setId(result.getInt(1));
                t.setNom(result.getString(3));
                t.setIdCoach(result.getInt(4));
                t.setIdRegion(result.getInt(5));
                t.setPays(result.getString(6));
                t.setCategorie(result.getBoolean(7));
            }
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return t;
    }


}
