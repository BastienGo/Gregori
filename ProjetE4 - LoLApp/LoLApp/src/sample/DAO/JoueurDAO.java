package sample.DAO;

import sample.Tables.Coach;
import sample.Tables.Joueur;
import sample.Tables.Licencié;
import sample.Tables.Team;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class JoueurDAO extends DAO {

    public void insertJoueur(Joueur j) {
        try {
            String req1 = "INSERT INTO joueur (pseudo, lic, nom, prenom, role, prin_sub) VALUES (?,?,?,?,?,?)";
            PreparedStatement pstmt = getConnection().prepareStatement(req1);

            pstmt.setString(1, j.getPseudo());
            pstmt.setString(2, j.getLic());
            pstmt.setString(3, j.getNom());
            pstmt.setString(4, j.getPrenom());
            pstmt.setString(5, j.getRole());
            pstmt.setString(6, j.getPrin_sub());
            int res2 = pstmt.executeUpdate();
            System.out.println("nb de modif réalisées : " + res2);
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteJoueur(String lic) {
        try {
            String req5 = "DELETE FROM joueur WHERE lic = ?";
            PreparedStatement supr = getConnection().prepareStatement(req5);
            supr.setString(1, lic);
            int res3 = supr.executeUpdate();
            System.out.println("supr : " + res3);
            supr.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void addJoueurToTeam(Joueur j, Coach co) throws SQLException {
        String req1 = "UPDATE joueurs SET idTeam = ? WHERE id = ? AND idTeam = null";
        PreparedStatement pstmt = getConnection().prepareStatement(req1);

        pstmt.setInt(1,j.getId());
        pstmt.setInt(1,co.getId());
        int res1 = pstmt.executeUpdate();
        System.out.println(res1);
    }

    public void delJoueurFromTeam(Joueur j) throws SQLException {
        String req2 = "UPDATE joueurs SET idTeam = null WHERE id = ?";
        PreparedStatement pstmt = getConnection().prepareStatement(req2);

        pstmt.setInt(1, j.getId());
        int res2 = pstmt.executeUpdate();
        System.out.println(res2);
    }

    /**
     * Méthode qui permet de rechercher un joueur dans la base de données en fonction de son id
     *
     * @param x id du joueur
     * @return res
     */

    public Joueur getJoueurByNick(String x) {
        Joueur res = null;
        Team t = null;
        Licencié l = null;
        try {
            Connection conn = super.getConnection();
            String req = "SELECT * FROM joueur, licencie, team WHERE joueur.lic = licencie.lic AND joueur.idTeam = team.id AND pseudo = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, x);
            ResultSet result = pstmt.executeQuery();
            while (result.next()) {
                res = new Joueur();
                t = new Team();
                l = new Licencié();
                t.setNom(result.getString(11));
                res.setTeam(t);
                l.setNom(result.getString(8));
                res.setPseudo(result.getString(2));
                res.setPrenom(result.getString(3));
                res.setRole(result.getString(4));
                res.setPrin_sub(result.getString(5));
            }
            result.close();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public Joueur getJoueurBylic(String x) {
        Joueur res = null;
        try {
            Connection conn = super.getConnection();
            System.out.println("connected");
            String req = "SELECT * FROM joueur, licencie WHERE joueur.lic = licencie.lic AND joueur.lic = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, x);
            ResultSet result = pstmt.executeQuery();
            if(result.next()){
                res = new Joueur();
                res.setNom(result.getString("nom"));
                res.setPrenom(result.getString(3));
                res.setPseudo(result.getString(2));
                res.setRole(result.getString(4));
                res.setPrin_sub(result.getString(5));
            }
            result.close();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }


    public ArrayList<Joueur> getJoueursByTeamName(String x) {
        ArrayList<Joueur> list = new ArrayList<>();
        Joueur res = null;
        try {
            Connection conn = super.getConnection();
            String req = "SELECT * FROM joueur, team WHERE joueur.idTeam = team.id AND team.nom = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, x);
            ResultSet result = pstmt.executeQuery();
            while (result.next()) {
                res = new Joueur();
                res.setPseudo(result.getString(3));
                res.setRole(result.getString(5));
                res.setPrenom(result.getString(4));
                res.setPrin_sub(result.getString(6));
                list.add(res);
            }
            result.close();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Joueur> getJoueursByCoach(int x) {
        ArrayList<Joueur> list = new ArrayList<>();
        Joueur res = null;
        try {
            Connection conn = super.getConnection();
            String req = "SELECT * FROM joueur, team, coach WHERE joueur.idTeam = team.id AND coach.id = team.idCoach AND coach.id = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setInt(1, x);
            ResultSet result = pstmt.executeQuery();
            while (result.next()) {
                res = new Joueur();
                res.setRole(result.getString(4));
                res.setPseudo(result.getString(2));
                res.setPrenom(result.getString(3));
                res.setPrin_sub(result.getString(5));
                list.add(res);
            }
            result.close();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Joueur> getJoueursByTeamTag(String x) {
        ArrayList<Joueur> list = new ArrayList<>();
        Joueur res = null;
        try {
            Connection conn = super.getConnection();
            System.out.println(x);
            String req = "SELECT * FROM joueur j, team t WHERE j.idTeam = t.id AND tag = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, x);
            ResultSet result = pstmt.executeQuery();
            while (result.next()) {
                res = new Joueur();
                res.setPseudo(result.getString(2));
                res.setPrenom(result.getString(3));
                res.setRole(result.getString(4));
                res.setPrin_sub(result.getString(5));
                list.add(res);
            }
            result.close();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Joueur> getJoueursByTeam(int x) {
        ArrayList<Joueur> list = new ArrayList<>();
        Joueur res = null;
        try {
            Connection conn = super.getConnection();
            String req = "SELECT * FROM joueur, team WHERE joueur.idTeam = team.id AND team.id = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setInt(1, x);
            ResultSet result = pstmt.executeQuery(req);
            while (result.next()) {
                res = new Joueur();
                res.setPseudo(result.getString(3));
                res.setPrenom(result.getString(4));
                res.setRole(result.getString(5));
                res.setPrin_sub(result.getString(6));
                list.add(res);
            }
            result.close();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public boolean updatePrenom(String prenom, String lic){
        Connection conn;
        boolean r = false;
        try{
            conn = super.getConnection();
            String req = "UPDATE joueur SET prenom = ? WHERE lic = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,prenom);
            pstmt.setString(2,lic);
            r = pstmt.executeUpdate() == 1;
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return r;
    }

    public boolean updatePseudo(String pseudo, String lic){
        Connection conn;
        boolean r = false;
        try{
            conn = super.getConnection();
            String req = "UPDATE joueur SET pseudo = ? WHERE lic = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,pseudo);
            pstmt.setString(2,lic);
            r = pstmt.executeUpdate() == 1;
            pstmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return r;
    }

    public boolean updateRole(String role, String lic) {
        Connection conn;
        boolean r = false;
        try {
            conn = super.getConnection();
            String req = "UPDATE joueur SET role = ? WHERE lic = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, role);
            pstmt.setString(2, lic);
            r = pstmt.executeUpdate() == 1;
            pstmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return r;
    }
}
