package sample.DAO;

import sample.Tables.Licencié;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DAO {
    private String driver = "com.mysql.cj.jdbc.Driver";
    private String url = "jdbc:mysql://localhost/e4_lolapp?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private String user = "root";
    private String pwd = "";

    /**
     * Méthode qui permet de se connecter à la base de données
     *
     * @return conn
     */

    protected Connection getConnection() {
        Connection conn = null;
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, pwd);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    public Licencié login(String nomL,String licL) {
        Connection conn;
        Licencié l = null;
        try {
            conn = getConnection();
            String req = "SELECT * FROM licencie WHERE nom = ? AND lic = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(2, licL);
            pstmt.setString(1, nomL);
            ResultSet result = pstmt.executeQuery();
            if(result.next()){
                l = new Licencié();
                l.setLic(result.getString(1));
                l.setNom(result.getString(2));
                l.setType(result.getInt(3));
            }
            pstmt.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return l;

    }
}

