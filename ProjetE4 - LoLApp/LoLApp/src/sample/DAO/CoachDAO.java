package sample.DAO;

import sample.Tables.Coach;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CoachDAO extends DAO {

    public Coach getCoachBylic(String x) {
        Coach res = null;
        try {
            Connection conn = super.getConnection();
            System.out.println("connected");
            String req = "SELECT * FROM coach, licencie WHERE coach.lic = licencie.lic AND coach.lic = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, x);
            ResultSet result = pstmt.executeQuery();
            if(result.next()){
                res = new Coach();
                res.setNom(result.getString("nom"));
                res.setPrenom(result.getString("prenom"));
                res.setPseudo(result.getString("pseudo"));
                res.setId(result.getInt(1));
            }
            result.close();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public boolean updatePrenom(String prenom, String lic){
        Connection conn;
        boolean r = false;
        try{
            conn = super.getConnection();
            String req = "UPDATE coach SET prenom = ? WHERE lic = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,prenom);
            pstmt.setString(2,lic);
            r = pstmt.executeUpdate() == 1;
            pstmt.close();
            conn.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return r;
    }

    public boolean updatePseudo(String pseudo, String lic){
        Connection conn;
        boolean r = false;
        try{
            conn = super.getConnection();
            String req = "UPDATE coach SET pseudo = ? WHERE lic = ?;";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1,pseudo);
            pstmt.setString(2,lic);
            r = pstmt.executeUpdate() == 1;
            pstmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return r;
    }



}
