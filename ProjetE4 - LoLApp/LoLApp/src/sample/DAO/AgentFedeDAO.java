package sample.DAO;

import sample.Tables.AgentFede;
import sample.Tables.Competition;
import sample.Tables.Joueur;

import java.sql.*;

public class AgentFedeDAO extends DAO {

    public AgentFede getAFBylic(String x) {
        AgentFede res = null;
        try {
            Connection conn = super.getConnection();
            System.out.println("connected");
            String req = "SELECT * FROM agentfede, licencie WHERE agentfede.lic = licencie.lic AND agentfede.lic = ?";
            PreparedStatement pstmt = conn.prepareStatement(req);
            pstmt.setString(1, x);
            ResultSet result = pstmt.executeQuery();
            if(result.next()){
                res = new AgentFede();
                res.setNom(result.getString("nom"));
            }
            result.close();
            pstmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
