package sample.Tables;

public class Champions {
    private int id;
    private String nom;

    public Champions() {
    }

    public Champions(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom(String string) {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Champions{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                '}';
    }
}
