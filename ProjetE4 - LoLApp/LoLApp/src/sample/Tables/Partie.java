package sample.Tables;

public class Partie {
    private int id;
    private int idTeam1;
    private int idTeam2;
    private int idComp;
    private String url;

    public Partie() {
    }

    public Partie(int id, int idTeam1, int idTeam2, int idComp, String url) {
        this.id = id;
        this.idTeam1 = idTeam1;
        this.idTeam2 = idTeam2;
        this.idComp = idComp;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIdComp() {
        return idComp;
    }

    public void setIdComp(int idComp) {
        this.idComp = idComp;
    }

    public int getIdTeam2() {
        return idTeam2;
    }

    public void setIdTeam2(int idTeam2) {
        this.idTeam2 = idTeam2;
    }

    public int getIdTeam1() {
        return idTeam1;
    }

    public void setIdTeam1(int idTeam1) {
        this.idTeam1 = idTeam1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Partie{" +
                "id=" + id +
                ", idTeam1=" + idTeam1 +
                ", idTeam2=" + idTeam2 +
                ", idComp=" + idComp +
                ", url='" + url + '\'' +
                '}';
    }
}
