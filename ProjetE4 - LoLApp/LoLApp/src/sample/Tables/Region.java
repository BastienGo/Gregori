package sample.Tables;

public class Region {
    private int id;
    private String libelle;
    private String tag;

    public Region(int id, String libelle, String tag) {
        this.id = id;
        this.libelle = libelle;
        this.tag = tag;
    }

    public Region() {
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Region{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                ", tag='" + tag + '\'' +
                '}';
    }
}
