package sample.Tables;

public class Team {
    private int id;
    private String nom;
    private String tag;
    private int idCoach;
    private int idRegion;
    private String pays;
    private boolean categorie;

    public Team() {
    }

    public Team(int id, String nom, String tag, int idCoach, int idRegion, String pays, boolean categorie) {
        this.id = id;
        this.nom = nom;
        this.tag = tag;
        this.idCoach = idCoach;
        this.idRegion = idRegion;
        this.pays = pays;
        this.categorie = categorie;
    }


    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", tag='" + tag + '\'' +
                ", idCoach=" + idCoach +
                ", idRegion=" + idRegion +
                ", pays='" + pays + '\'' +
                ", categorie=" + categorie +
                '}';
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public boolean isCategorie() {
        return categorie;
    }

    public void setCategorie(boolean categorie) {
        this.categorie = categorie;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public int getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(int idRegion) {
        this.idRegion = idRegion;
    }

    public int getIdCoach() {
        return idCoach;
    }

    public void setIdCoach(int idCoach) {
        this.idCoach = idCoach;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean getCategorie() {return categorie;
    }

    public void setCategorie(Boolean categorie){this.categorie = categorie;}
}
