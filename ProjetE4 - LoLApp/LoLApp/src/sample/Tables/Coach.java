package sample.Tables;

public class Coach extends Licencié {
    private int id;
    private String pseudo;
    private String prenom;
    private String idTeam;

    public Coach(int id, String pseudo, String prenom, String idTeam, String lic, String nom) {
        super(lic, nom);
        this.id = id;
        this.pseudo = pseudo;
        this.prenom = prenom;
        this.idTeam = idTeam;
    }

    public Coach() {
    }

    public String getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(String idTeam) {
        this.idTeam = idTeam;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    @Override
    public String toString() {
        return "Coach{" +
                "id=" + id +
                ", lic='" + lic +
                ", nom='" + nom + '\'' +
                ", pseudo='" + pseudo + '\'' +
                ", prenom='" + prenom + '\'' +
                ", idTeam='" + idTeam + '\'' +
                '}';
    }
}
