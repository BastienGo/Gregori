package sample.Tables;

public class Joueur extends Licencié{
    private int id;
    private String pseudo;
    private String prenom;
    private String role;
    private String prin_sub;
    private Team team;

    public Joueur(int id, String pseudo, String prenom, String role, String prin_sub, int idTeam, String lic, String nom) {
        super(lic,nom);
        this.id = id;
        this.pseudo = pseudo;
        this.prenom = prenom;
        this.role = role;
        this.prin_sub = prin_sub;
        this.team = team;
    }

    public Joueur() {
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public String getPrin_sub() {
        return prin_sub;
    }

    public void setPrin_sub(String prin_sub) {
        this.prin_sub = prin_sub;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Joueur{" +
                "id=" + id +
                ", pseudo='" + pseudo + '\'' +
                ", prenom='" + prenom + '\'' +
                ", role='" + role + '\'' +
                ", prin_sub='" + prin_sub + '\'' +
                ", team=" + team +
                '}';
    }
}
