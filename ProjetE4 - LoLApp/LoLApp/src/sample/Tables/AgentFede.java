package sample.Tables;

public class AgentFede extends Licencié {
    private int id;

    public AgentFede() {

    }

    public AgentFede(String lic, String nom, int id) {
        super(lic, nom, 3);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AgentFede{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", lic='" + lic + '\'' +
                '}';
    }
}
