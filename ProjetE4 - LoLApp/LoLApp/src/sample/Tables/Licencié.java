package sample.Tables;

public class Licencié {
    protected String lic;
    protected String nom;
    protected int type;

    public Licencié(){

    }

    public Licencié(String lic, String nom){
        this.lic = lic;
        this.nom = nom;
    }

    public Licencié(String lic, String nom, int type) {
        this.lic = lic;
        this.nom = nom;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getLic() {
        return lic;
    }

    public void setLic(String lic) {
        this.lic = lic;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Licencié{" +
                "lic='" + lic + '\'' +
                ", nom='" + nom + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
