package sample.Panels;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import sample.DAO.JoueurDAO;
import sample.Main;
import sample.Tables.Joueur;
import sample.Tables.Licencié;

public class JoueurTab extends GridPane {

    private Text nom = new Text("");
    private Text prenom = new Text("");
    private Text pseudo = new Text("");
    private Text role = new Text("");

    private Licencié licencie = null;

    public void setLicencie(Licencié licencie){
        this.licencie = licencie ;
        init();
    }

    public Licencié getLicencie(){
        return licencie;
    }


    private void init(){
        JoueurDAO jdao = new JoueurDAO();
        String lic = licencie.getLic();
        System.out.println(lic);
        Joueur j = jdao.getJoueurBylic(lic);

        nom.setText(j.getNom());
        prenom.setText(j.getPrenom());
        pseudo.setText(j.getPseudo());
        role.setText(j.getRole());

        Button btnJoueur = new Button("Modifier vos informations");

        this.add(btnJoueur,1,6);
        btnJoueur.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                UpdJoueurTab updjt = (UpdJoueurTab) Main.getPanel("updjoueurtab");
                updjt.setJ(j);
                Main.getScene().setRoot(updjt);
            }
        });
    }

    public JoueurTab() {

        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));



        Button btnRet = new Button("Retour");


        this.add(nom, 1,1);
        this.add(prenom, 1,2);
        this.add(pseudo, 1,3);
        this.add(role, 1,4);
        this.add(btnRet,2,6);

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LogTab.dc();

            }
        });
    }
}