package sample.Panels;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import sample.DAO.JoueurDAO;
import sample.Main;
import sample.Tables.Joueur;
import sample.Tables.Licencié;

public class AddDelUpdCompetitionTab extends GridPane {

    private void init(){

    }

    public AddDelUpdCompetitionTab() {

        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));


        // Voir / Modifier les infos d'une competition


        Button btnRet = new Button("Retour");


        this.add(btnRet,2,6);

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("coachtab"));
            }
        });
    }
}