package sample.Panels;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import sample.DAO.JoueurDAO;
import sample.DAO.TeamDAO;
import sample.Main;
import sample.Tables.Joueur;
import sample.Tables.Team;

import java.util.ArrayList;

public class RechJoueurTab extends GridPane {
    private Text nom = new Text("");
    private Text prenom = new Text("");
    private Text pseudo = new Text("");
    private Text role = new Text("");
    private Text prin_sub = new Text("");
    private Text team = new Text("");
    TextField search = new TextField();

    private void init(){
        JoueurDAO jdao = new JoueurDAO();
        Joueur j = jdao.getJoueurByNick(search.getText());

        nom.setText(j.getNom());
        prenom.setText(j.getPrenom());
        pseudo.setText(j.getPseudo());
        role.setText(j.getRole());
        prin_sub.setText(j.getPrin_sub());
        team.setText(j.getTeam().getNom());

        Label zonenom = new Label("Nom");
        Label zoneprenom = new Label("Prénom");
        Label zonepseudo = new Label("Pseudo");
        Label zonerole = new Label("Role");
        Label zoneprinsub = new Label("Prin_sub");
        Label zoneteam = new Label("Team");


        this.add(zonenom, 1, 2);
        this.add(zoneprenom, 1, 3);
        this.add(zonepseudo, 1, 4);
        this.add(zonerole, 1, 5);
        this.add(zoneprinsub, 1, 6);
        this.add(zoneteam, 1, 7);
    }


    public RechJoueurTab(){
        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        String name = new String();

        Button btnVal = new Button("Rechercher");
        Button btnRet = new Button("Retour");

        Label text = new Label("Pseudo du joueur");
        this.add(text, 1, 1);

        this.add(search,2,1);
        this.add(btnVal, 3,1);


        this.add(btnRet, 0, 1);




        this.add(nom, 2,2);
        this.add(prenom, 2,3);
        this.add(pseudo, 2,4);
        this.add(role, 2,5);
        this.add(prin_sub, 2, 6);
        this.add(team, 2, 7);



        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("publictab"));
            }
        });

        btnVal.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                init();
            }
        });


        this.setPadding(new Insets(5));
    }
}
