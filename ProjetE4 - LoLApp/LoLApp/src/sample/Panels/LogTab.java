package sample.Panels;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import sample.DAO.DAO;
import sample.Main;
import sample.Tables.Joueur;
import sample.Tables.Licencié;

import static javafx.geometry.HPos.RIGHT;


public class LogTab extends GridPane {


    public static void dc() {
        Main.getScene().setRoot(Main.getPanel("logtab"));
        LogTab.nameKeep = null;
        LogTab.licKeep = null;
        nameField.setText("");
        licBox.setText("");
    }

    public static String nameKeep;
    public static String licKeep;
    private static PasswordField licBox = new PasswordField();
    private static TextField nameField = new TextField();


    public LogTab() {
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        DAO dao = new DAO();

        Text scenetitle = new Text("Connexion");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        this.add(scenetitle, 0, 0, 2, 1);

        Label nom = new Label("Nom:");
        Label lic = new Label("Licence:");

        this.add(nom, 0, 1);
        this.add(nameField, 1, 1);
        this.add(lic, 0, 2);
        this.add(licBox, 1, 2);

        Button btn = new Button("Connexion");
        Button btnPublic = new Button("Public");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btnPublic);
        hbBtn.getChildren().add(btn);
        this.add(hbBtn, 1, 4);

        final Text actiontarget = new Text();
        this.add(actiontarget, 0, 6);
        this.setColumnSpan(actiontarget, 2);
        this.setHalignment(actiontarget, RIGHT);
        actiontarget.setId("actiontarget");

        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String nom = nameField.getText();
                String lic = licBox.getText();
                DAO oui = new DAO();
                Licencié test = oui.login(nom,lic);
                if (test.getType()== 2) {
                    nameKeep = nom;
                    licKeep = lic;
                    CoachTab ctab = (CoachTab) Main.getPanel("coachtab");
                    ctab.setLicencie(test);
                    Main.getScene().setRoot(ctab);
                } else if (test.getType() == 1) {
                    nameKeep = nom;
                    licKeep = lic;
                    AFTab aftab = (AFTab) Main.getPanel("aftab");
                    aftab.setLicencie(test);
                    Main.getScene().setRoot(aftab);
                }
                else if(test.getType() == 3){
                    nameKeep = nom;
                    licKeep = lic;
                    JoueurTab jtab = (JoueurTab) Main.getPanel("joueurtab");
                    jtab.setLicencie(test);
                    Main.getScene().setRoot(jtab);
                }
            }
        });

        btnPublic.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("publictab"));
            }
        });
    }
}