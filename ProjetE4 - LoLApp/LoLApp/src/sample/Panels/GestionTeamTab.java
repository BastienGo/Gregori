package sample.Panels;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import sample.DAO.JoueurDAO;
import sample.DAO.TeamDAO;
import sample.Main;
import sample.Tables.Coach;
import sample.Tables.Joueur;
import sample.Tables.Licencié;
import sample.Tables.Team;

import java.util.ArrayList;

public class GestionTeamTab extends GridPane {

    private TableView<Joueur> tablej = new TableView<>();
    JoueurDAO jdao = new JoueurDAO();
    private Coach coach = null;
    private TeamDAO tdao = null;

    public void setCoach(Coach coach){
        this.coach = coach;
        init();
    }

    public Coach getCoach(){
        return coach;
    }


    private void init(){
        TableColumn<Joueur, String> pseudo = new TableColumn<>("Pseudo");
        TableColumn<Joueur, String> role = new TableColumn<>("Rôle");
        TableColumn<Joueur, String> prin_sub = new TableColumn<>("Principal_Sub");

        tablej.getColumns().addAll(pseudo, role, prin_sub);

        pseudo.setCellValueFactory(new PropertyValueFactory<>("pseudo"));
        role.setCellValueFactory(new PropertyValueFactory<>("role"));
        prin_sub.setCellValueFactory(new PropertyValueFactory<>("prin_sub"));

        this.setPadding(new Insets(5));
        this.getChildren().add(tablej);


        System.out.println(coach.getId());
        ArrayList<Joueur> listj = jdao.getJoueursByCoach(coach.getId());
        ObservableList list2 = FXCollections.observableArrayList(listj);
        System.out.println(list2);
        tablej.setItems(list2);
    }

    public GestionTeamTab() {

        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));






        Button btnRet = new Button("Retour");


        this.add(btnRet,2,6);

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("coachtab"));
            }
        });
    }
}