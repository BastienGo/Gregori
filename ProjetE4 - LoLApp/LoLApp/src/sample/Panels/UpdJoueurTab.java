package sample.Panels;

import com.mysql.cj.log.Log;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import sample.DAO.JoueurDAO;
import sample.DAO.LicenciéDAO;
import sample.Main;
import sample.Tables.Joueur;
import sample.Tables.Licencié;

public class UpdJoueurTab extends GridPane {

    private Text nomatm = new Text("");
    private Text prenomatm = new Text("");
    private Text pseudoatm = new Text("");
    private Text roleatm = new Text("");


    private Joueur j;

    public void setJ(Joueur j){
        this.j = j;
        init();
    }

    public Joueur getJ(){
        return j;
    }



    private void init(){
        nomatm.setText(j.getNom());
        prenomatm.setText(j.getPrenom());
        pseudoatm.setText(j.getPseudo());
        roleatm.setText(j.getRole());
    }


    public UpdJoueurTab() {

        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25,25,25,25));

        LicenciéDAO ldao = new LicenciéDAO();
        JoueurDAO jdao = new JoueurDAO();


        Text saisie1 = new Text("Saisissez le nouveau nom");
        TextField newnom = new TextField();
        Text saisie2 = new Text("Saisissez le nouveau prenom");
        TextField newprenom = new TextField();
        Text saisie3 = new Text("Saisissez le nouveau pseudo");
        TextField newpseudo = new TextField();
        Text saisie4 = new Text("Saisissez le nouveau role");
        TextField newrole = new TextField();

        Button btnVal = new Button("Valider");
        Button btnRet = new Button("Retour");


        this.add(saisie1,1,1);
        this.add(newnom,2,1);
        this.add(saisie2,1,2);
        this.add(newprenom,2,2);
        this.add(saisie3,1,3);
        this.add(newpseudo,2,3);
        this.add(saisie4,1,4);
        this.add(newrole,2,4);
        this.add(btnVal, 1,5);
        this.add(btnRet, 3,5);


        this.add(nomatm, 3,1);
        this.add(prenomatm, 3,2);
        this.add(pseudoatm, 3,3);
        this.add(roleatm, 3,4);

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("joueurtab"));
            }
        });


        btnVal.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (newprenom.getText().equals("")){}
                else {
                    boolean r = jdao.updatePrenom(newprenom.getText(), LogTab.licKeep);
                    if(r){
                        j.setPrenom(newprenom.getText());
                    }
                }if (newnom.getText().equals("")){}
                else {
                    boolean r = ldao.updateNom(newnom.getText(), LogTab.licKeep);
                    if(r){
                        j.setNom(newnom.getText());
                    }
                }if (newpseudo.getText().equals("")){}
                else {
                    boolean r = jdao.updatePseudo(newpseudo.getText(), LogTab.licKeep);
                    if(r){
                        j.setPseudo(newpseudo.getText());
                    }
                }if (newrole.getText().equals("")){}
                else {
                    boolean r = jdao.updateRole(newrole.getText(), LogTab.licKeep);
                    if(r){
                        j.setRole(newrole.getText());
                    }
                }
                init();
            }
        });


    }
}
