package sample.Panels;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import sample.Main;

public class PublicTab extends GridPane {
    public PublicTab(){

        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        Button btnJoueurs = new Button("Rechercher un joueur");
        Button btnTeam = new Button("Rechercher une équipe");
        Button btnRet = new Button("Retour");

        this.add(btnJoueurs,1,1);
        this.add(btnTeam,2,1);
        this.add(btnRet,1,2);


        btnJoueurs.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("rechjoueurtab"));
                // Faire disparaitre les boutons
                // Apparaitre une barre de recherche et un bouton rechercher
                // // Bouton rechercher affiche les informations du joueur
            }
        });

        btnTeam.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("rechteamtab"));
            }
        });


        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("logtab"));
            }
        });
    }

}
