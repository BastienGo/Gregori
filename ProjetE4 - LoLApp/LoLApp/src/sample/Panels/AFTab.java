package sample.Panels;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import sample.DAO.AgentFedeDAO;
import sample.Main;
import sample.Tables.AgentFede;
import sample.Tables.Joueur;
import sample.Tables.Licencié;

public class AFTab extends GridPane {


    private Text nom = new Text("");

    private Licencié licencie = null;

    public void setLicencie(Licencié licencie){
        this.licencie = licencie ;
        init();
    }

    public Licencié getLicencie(){
        return licencie;
    }

    private void init(){
        AgentFedeDAO afdao = new AgentFedeDAO();
        String lic = licencie.getLic();
        System.out.println(lic);
        Licencié licu = afdao.getAFBylic(lic);

        nom.setText(licu.getNom());
    }

    public AFTab(){




        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));


        Button btnCoach = new Button("Accéder aux coach");
        Button btnJoueur = new Button("Accéder aux Joueurs");
        Button btnComp = new Button("Accéder aux Compétitions");
        Button btnPartie = new Button("Accéder aux Parties");
        Button btnRet = new Button("Retour");

        this.add(nom, 1,1);
        this.add(btnCoach,1,2);
        this.add(btnJoueur,2,2);
        this.add(btnComp, 1,3);
        this.add(btnPartie, 2,3);
        this.add(btnRet,1,4);

        btnCoach.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("AddDelCoachTab"));
            }
        });

        btnJoueur.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("AddDelJoueurTab"));
            }
        });

        btnComp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("AddDelUpdCompetitionTab"));
            }
        });

        btnPartie.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("AddDelUpdPartieTab"));
            }
        });
        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LogTab.dc();
            }
        });
    }
}
