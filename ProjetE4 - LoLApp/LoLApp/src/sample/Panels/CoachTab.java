package sample.Panels;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import sample.DAO.AgentFedeDAO;
import sample.DAO.CoachDAO;
import sample.Main;
import sample.Tables.Coach;
import sample.Tables.Licencié;

public class CoachTab extends GridPane {

    private Text nom = new Text("");
    private Text prenom = new Text("");
    private Text pseudo = new Text("");

    private Licencié licencie = null;
    private Coach c = null;

    public void setLicencie(Licencié licencie){
        this.licencie = licencie ;
        init();
    }

    public Licencié getLicencie(){
        return licencie;
    }

    private void init(){
        CoachDAO cdao = new CoachDAO();
        String lic = licencie.getLic();
        System.out.println(lic);
        Licencié licu = cdao.getCoachBylic(lic);
        c = cdao.getCoachBylic(lic);

        nom.setText(licu.getNom());
        prenom.setText(c.getPrenom());
        pseudo.setText(c.getPseudo());
    }

    public CoachTab(){

        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));



        Button btnTeam = new Button("Accéder à son équipe");
        Button btnJoueur = new Button("Accéder à ses Joueurs");
        Button btnComp = new Button("Accéder aux Compétitions");
        Button btnInfos = new Button("Modifier vos infos");
        Button btnRet = new Button("Retour");

        this.add(nom, 1,1);
        this.add(prenom, 1,2);
        this.add(pseudo, 1,3);
        this.add(btnTeam,3,1);
        this.add(btnJoueur,3,2);
        this.add(btnComp, 3,3);
        this.add(btnInfos, 3,4);
        this.add(btnRet,1,4);

        btnTeam.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("updteamtab"));
            }
        });

        btnJoueur.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                GestionTeamTab gtt = (GestionTeamTab) Main.getPanel("gestionteamtab");
                gtt.setCoach(c);
                System.out.println(c.getId());
                Main.getScene().setRoot(gtt);
            }
        });

        btnComp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("accescomptab"));
            }
        });

        btnInfos.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                UpdCoachTab uct = (UpdCoachTab) Main.getPanel("updcoachtab");
                uct.setC(c);
                Main.getScene().setRoot(uct);
            }
        });
        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LogTab.dc();
            }
        });
    }
}

