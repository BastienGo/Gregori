package sample.Panels;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import sample.DAO.JoueurDAO;
import sample.DAO.TeamDAO;
import sample.Main;
import sample.Tables.Joueur;
import sample.Tables.Team;

import java.util.ArrayList;

public class RechTeamTab extends GridPane {
    private TableView<Joueur> tablej = new TableView<>();
    JoueurDAO jdao = new JoueurDAO();
    TextField search = new TextField();
    TextField search2 = new TextField();

    private void init(){
        ArrayList<Joueur> listj = jdao.getJoueursByTeamName(search.getText());
        ObservableList list2 = FXCollections.observableArrayList(listj);
        tablej.setItems(list2);
    }

    private void init2(){
        ArrayList<Joueur> listj = jdao.getJoueursByTeamTag(search2.getText());
        ObservableList list2 = FXCollections.observableArrayList(listj);
        tablej.setItems(list2);
    }


    public RechTeamTab(){
        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));

        String name = new String();
        TeamDAO tdao = new TeamDAO();

        Button btnVal = new Button("Rechercher");
        Button btnVal2 = new Button("Rechercher");
        Button btnRet = new Button("Retour");

        Label text = new Label("Nom de l'équipe");
        this.add(text, 1, 1);

        this.add(search,2,1);
        this.add(btnVal, 3,1);

        Label text2 = new Label("Tag de l'équipe");
        this.add(text2,1, 3);

        this.add(search2, 2, 3);
        this.add(btnVal2, 3,3);

        this.add(btnRet, 0, 1);


        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("publictab"));
            }
        });

        btnVal.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                init();
            }
        });

        btnVal2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                init2();
            }
        });

        TableColumn<Joueur, String> pseudo = new TableColumn<>("Pseudo");
        TableColumn<Joueur, String> role = new TableColumn<>("Rôle");
        TableColumn<Joueur, String> prin_sub = new TableColumn<>("Principal_Sub");

        tablej.getColumns().addAll(pseudo, role, prin_sub);

        pseudo.setCellValueFactory(new PropertyValueFactory<>("pseudo"));
        role.setCellValueFactory(new PropertyValueFactory<>("role"));
        prin_sub.setCellValueFactory(new PropertyValueFactory<>("prin_sub"));


        this.setPadding(new Insets(5));
        this.getChildren().add(tablej);



        // Afficher les informations de l'équipe recherchée à droite du tableau contenant les joueurs
    }
}
