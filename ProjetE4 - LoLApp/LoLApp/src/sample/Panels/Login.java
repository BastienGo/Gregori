package sample.Panels;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Login extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("JavaFX Welcome");

        Scene scene = new Scene(new StackPane(), 600, 550);
        LogTab grid = new LogTab();
        scene.setRoot(grid);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


}