package sample.Panels;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import sample.DAO.CoachDAO;
import sample.DAO.JoueurDAO;
import sample.DAO.LicenciéDAO;
import sample.Main;
import sample.Tables.Coach;
import sample.Tables.Joueur;
import sample.Tables.Licencié;

public class UpdCoachTab extends GridPane {

    private Text nomatm = new Text("");
    private Text prenomatm = new Text("");
    private Text pseudoatm = new Text("");

    private Coach c;

    public void setC(Coach c){
        this.c = c;
        init();
    }

    public Coach getC(){
        return c;
    }

    private void init(){
        prenomatm.setText(c.getPrenom());
        nomatm.setText(c.getNom());
        pseudoatm.setText(c.getPseudo());
    }

    public UpdCoachTab() {

        this.setAlignment(Pos.CENTER_LEFT);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));


        LicenciéDAO ldao = new LicenciéDAO();
        CoachDAO cdao = new CoachDAO();
        // Voir / Modifier les infos du coach


        Text saisie1 = new Text("Saisissez le nouveau nom");
        TextField newnom = new TextField();
        Text saisie2 = new Text("Saisissez le nouveau prenom");
        TextField newprenom = new TextField();
        Text saisie3 = new Text("Saisissez le nouveau pseudo");
        TextField newpseudo = new TextField();

        Button btnVal = new Button("Valider");
        Button btnRet = new Button("Retour");


        this.add(saisie1,1,1);
        this.add(newnom,2,1);
        this.add(saisie2,1,2);
        this.add(newprenom,2,2);
        this.add(saisie3,1,3);
        this.add(newpseudo,2,3);
        this.add(btnVal, 1,5);
        this.add(btnRet, 3,5);

        this.add(nomatm, 3,1);
        this.add(prenomatm, 3,2);
        this.add(pseudoatm, 3,3);

        btnRet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.getScene().setRoot(Main.getPanel("coachtab"));
            }
        });

        btnVal.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (newprenom.getText().equals("")){}
                else {
                    boolean r = cdao.updatePrenom(newprenom.getText(), LogTab.licKeep);
                    if(r){
                        c.setPrenom(newprenom.getText());
                    }
                }if (newnom.getText().equals("")){}
                else {
                    boolean r = ldao.updateNom(newnom.getText(), LogTab.licKeep);
                    if(r){
                        c.setNom(newnom.getText());
                    }
                }if (newpseudo.getText().equals("")){}
                else {
                    boolean r = cdao.updatePseudo(newpseudo.getText(), LogTab.licKeep);
                    if(r){
                        c.setPseudo(newpseudo.getText());
                    }
                }
                init();
            }
        });
    }
}