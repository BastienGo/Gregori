package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import sample.Panels.*;

import java.util.Hashtable;

public class Main extends Application {
    private static Scene scene;

    private static Hashtable<String, Pane> vues = new Hashtable<>();


    private static void initVue() {
        vues.put("accescomptab", new AccesCompTab());
        vues.put("adddelcoachtab", new AddDelCoachTab());
        vues.put("adddeljoueurtab", new AddDelJoueurTab());
        vues.put("adddelupdcompetitiontab", new AddDelUpdCompetitionTab());
        vues.put("adddelupdpartietab", new AddDelUpdPartieTab());
        vues.put("aftab", new AFTab());
        vues.put("coachtab", new CoachTab());
        vues.put("gestionteamtab", new GestionTeamTab());
        vues.put("joueurtab", new JoueurTab());
        vues.put("logtab", new LogTab());
        vues.put("publictab", new PublicTab());
        vues.put("updcoachtab", new UpdCoachTab());
        vues.put("updjoueurtab", new UpdJoueurTab());
        vues.put("updteamtab", new UpdTeamTab());
        vues.put("rechjoueurtab", new RechJoueurTab());
        vues.put("rechteamtab", new RechTeamTab());
    }


    public static Pane getPanel(String s) {
        return vues.get(s);
    }

    public static Scene getScene() {
        return scene;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        initVue();
        scene = new Scene(getPanel("logtab"), 700, 450);
        primaryStage.setTitle("LoLApp");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
