package eu.siohautil.heritage;

/** Classe Bateau
 *  @author BastienGre
 *  @version 1.0
 */

public class Bateau extends Vehicule implements Navigable {
    /**
     * volume est un entier (ex : 170)
     */
    private int volume;

    /**
     * Un constructeur à 4 paramètres, marque, modele, couleur et volume
     * @param marque
     * @param modele
     * @param couleur
     * @param volume
     */
    public Bateau(String marque, String modele, String couleur, int volume) {
        super(marque, modele, couleur);
        this.volume = volume;
    }

    /**
     * Une méthode qui affiche le modele du bateau qui accoste
     */
    public void accoster() {System.out.println("Le bateau " + this.getModele() + " accoste.");}

    /**
     * Une méthode qui affiche le modele du bateau qui navigue
     */
    public void naviguer() {System.out.println("Le bateau " + this.getModele() + " navigue.");}

    /**
     * Une méthode qui affiche le modele du bateau qui coule
     */
    public void couler() {System.out.println("Le bateau " + this.getModele() + " coule.");}

    /**
     * Une méthode qui affiche le modele du bateau qui démarre
     */
    @Override
    public void demarrer() {System.out.println("Le bateau " + this.getModele() + " démarre.");}

    /**
     * Une méthode qui affiche le modele du bateau qui freine
     */
    @Override
    public void freiner() {System.out.println("Le bateau " + this.getModele() + " freine.");}
}