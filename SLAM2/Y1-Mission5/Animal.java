package eu.siohautil.heritage;

/**Classe Animal
 * @author BastienGre
 * @version 1.0
 */

public class Animal {
    /**
     * surnom est une chaine de caractères (ex : Pilou)
     */
    private String surnom;
    /**
     * age est un entier (ex : 10)
     */
    private int age;
    /**
     * type est une chaine de caractères (ex : Malinois)
     */
    private String type;

    /**
     * Un constructeur vide
     */
    public Animal(){

    }

    /**
     * Un constructeur à trois paramètres, surnom, age et type
     * @param surnom
     * @param age
     * @param type
     */
    public Animal(String surnom, int age, String type) {
        this.surnom = surnom;
        this.age = age;
        this.type = type;
    }

    /**
     * Une méthode qui permet de changer le surnom
     * @param surnom
     */
    public void setSurnom(String surnom) {
        this.surnom = surnom;
    }

    /**
     * une méthode qui permet de changer l'age
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Une méthode qui permet de changer le type
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Une méthode qui récupère le surnom
     * @return la chaine surnom
     */
    public String getSurnom(){
        return this.surnom;
    }

    /**
     * Une méthode qui récupère l'age
     * @return l'entier age
     */
    public int getAge(){
        return this.age;
    }

    /**
     * Une méthode qui récupère le type
     * @return la chaine type
     */
    public String getType(){
        return this.type;
    }

    /**
     * Une méthode qui affiche le surnom et l'age de l'Animal
     */
    public void afficher(){
        System.out.println("L'animal " + getSurnom() +  " a " + getAge() + " ans.");
    }
}