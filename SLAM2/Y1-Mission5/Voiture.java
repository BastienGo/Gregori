package eu.siohautil.heritage;

/** Classe Voiture
 * @author BastienGre
 * @version 1.0
 */

public class Voiture extends Vehicule {
    /**
     * nbPlace est un entier (ex : 4)
     */
    private int nbPlace;

    /**
     * Un constructeur à 4 paramètres, marque, modele, couleur et nbPlace
     * @param marque
     * @param modele
     * @param couleur
     * @param nbPlace
     */
    public Voiture(String marque, String modele, String couleur, int nbPlace){
        super(marque, couleur, modele);
        this.nbPlace = nbPlace;
    }

    /**
     * Une méthode qui affiche la marque, le modele et la couleur de la voiture qui démarre
     */
    public void demarrer(){
        System.out.println("La voiture " + this.getMarque() + " " + this.getModele() + " " + this.getCouleur() + " démarre.");
    }

    /**
     * Une méthode qui affiche la marque, le modele et la couleur de la voiture qui freine
     */
    public void freiner(){
        System.out.println("La voiture " + this.getMarque() + " " + this.getModele() + " " + this.getCouleur() + " freine.");
    }

    /**
     * Une méthode qui affiche la marque, le modele et la couleur de la voiture qui stationne
     */
    public void stationner(){
        System.out.println("La voiture " + this.getMarque() + " " + this.getModele() + " " + this.getCouleur() + " stationne.");
    }
}
