package eu.siohautil.heritage;

/** Classe Vehicule
 * @author BastienGre
 * @version 1.0
 */

public abstract class Vehicule {
    /**
     * marque est une chaine de caractères (ex : Volkswagen)
     */
    private String marque;
    /**
     * couleur est une chaine de caractères (ex : Rouge)
     */
    private String couleur;
    /**
     * modele est une chaine de caractères (ex : Passat)
     */
    private String modele;

    /**
     * Un constructeur à 3 paramètres, marque, modèle et couleur
     * @param marque
     * @param modele
     * @param couleur
     */
    public Vehicule(String marque, String modele, String couleur) {
        this.marque = marque;
        this.modele = modele;
        this.couleur = couleur;
    }


    public abstract void demarrer();
    public abstract void freiner();

    /**
     * Une méthode qui récupère la couleur
     * @return la chaine couleur
     */
    public String getCouleur(){
        return couleur;
    }

    /**
     * Une méthode qui récupère la marque
     * @return la chaine marque
     */
    public String getMarque(){
        return marque;
    }

    /**
     * Une méthode qui récupère le modele
     * @return la chaine modele
     */
    public String getModele() {
        return modele;
    }
}
