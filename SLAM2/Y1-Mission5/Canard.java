package eu.siohautil.heritage;

/** Classe Canard
 *  @author BastienGre
 *  @version 1.0
 */
public class Canard extends Animal implements Navigable {
    /**
     * nbPlumes est un entier (ex : 9000)
     */
    private int nbPlumes;

    /**
     * Un constructeur à 4 paramètres, surnom, age, type et nbPlumes
     * @param surnom
     * @param age
     * @param type
     * @param nbPlumes
     */
    public Canard( String surnom, int age , String type, int nbPlumes ) {
        super(surnom, age, type);
        this.nbPlumes = nbPlumes ;
    }

    /**
     * Une méthode qui affiche le nom du canard qui accoste
     */
    public void accoster() {
        System.out.println ("Le canard "+ this.getSurnom() + " accoste.");
    }

    /**
     * Une méthode qui affiche le nom du canard qui navigue
     */
    public void naviguer() {
        System.out.println ("Le canard "+ this.getSurnom() + " navigue.");
    }

    /**
     * Une méthode qui affiche le nom du canard qui coule
     */
    public void couler() {
        System.out.println("Le canard " + this.getSurnom() + " coule.");
    }
}
