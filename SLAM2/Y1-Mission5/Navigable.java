package eu.siohautil.heritage;

public interface Navigable{
    public void accoster();
    public void naviguer();
    public void couler();
}