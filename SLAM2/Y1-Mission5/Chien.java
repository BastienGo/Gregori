package eu.siohautil.heritage;

/** Classe Chien
 * @author BastienGre
 * @version 1.0
 */

public class Chien extends Animal{
    /**
     * couleur est une chaine de caractères (ex : Blanc)
     */
    private String couleur;

    /**
     * Un constructeur à 4 paramètres, surnom, type, age et couleur
     * @param surnom
     * @param type
     * @param age
     * @param couleur
     */
    public Chien(String surnom, String type, int age, String couleur) {
        super(surnom, age, type);
        this.couleur = couleur;
    }

    /**
     * Une méthode qui permet d'afficher le surnom du chien qui aboie
     */
    public void aboyer(){
        System.out.println("Le chien "+ this.getSurnom() + " aboie.");
    }
}
