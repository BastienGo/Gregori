package eu.siohautil.test;

import eu.siohautil.base.AgenceVoyage;
import eu.siohautil.base.AdressePostale;
import eu.siohautil.base.Bagage;
import eu.siohautil.base.Voyageur;
import eu.siohautil.heritage.VoyageurHandicape;
import eu.siohautil.heritage.VoyageurPrivilege;
import java.util.ArrayList;
import java.util.Scanner;

public class Mission4 {
    private static Scanner sc;

    public static void main (String []  args) {
        sc = new Scanner(System.in);
        ArrayList<Voyageur> voys = new ArrayList<Voyageur>();
        AgenceVoyage agv = new AgenceVoyage();
        System.out.println("Quel est le nom de votre agence de voyage ?");
        String nomav = sc.nextLine();

        System.out.println("Quelle est son adresse ? (N° et nom de rue)");
        String numrueav = sc.nextLine();
        System.out.println("Quelle est la ville ou elle se situe ?");
        String villeav = sc.nextLine();
        System.out.println("Quel est le code postal de la ville ?");
        String cpav = sc.nextLine();

        String handi = "";
        int codep = 0;
        AdressePostale adresseav = new AdressePostale();
        adresseav.setAdresse(numrueav);
        adresseav.setCP(cpav);
        adresseav.setVille(villeav);

        agv.setAdresse(adresseav);
        agv.setNom(nomav);

        System.out.println("Que souhaitez vous faire ? 1 pour ajouter un voyageur, 2 pour rechercher un voyageur, 3 pour supprimer un voyageur, 4 pour afficher les informations, et 5 pour quitter l'application :");
        int appli = sc.nextInt();

        while (appli != 5) {
            if (appli == 1) {
                System.out.println("Ce voyageur est-il spécifique ? 1 pour oui, 0 pour non");
                int testspe = sc.nextInt();
                if (testspe == 1) {
                    System.out.println("Saisissez le code privilège : ");
                    codep = sc.nextInt();
                }

                System.out.println("Ce voyageur est-il handicapé ? 1 pour oui, 0 pour non");
                int testhandi = sc.nextInt();
                if (testhandi == 1) {
                    System.out.println("Saisissez l'handicap du voyageur :");
                    handi = sc.next();
                }

                AdressePostale x = new AdressePostale();
                Bagage a = new Bagage();
                Voyageur v = new Voyageur();
                System.out.println(v.toString());

                System.out.println("Saisissez votre nom: ");
                String nom = sc.next();
                v.setNom(nom);

                System.out.println("Quel est votre âge ?");
                int age = sc.nextInt();
                v.setAge(age);
                sc.nextLine();

                System.out.println("Saisissez votre adresse: ");
                String adresse = sc.nextLine();
                x.setAdresse(adresse);

                System.out.println("Saisissez votre ville: ");
                String ville = sc.nextLine();
                x.setVille(ville);

                System.out.println("Saisissez votre code postal: ");
                String codePostale = sc.nextLine();
                x.setCP(codePostale);

                v.setAdresse(x);

                System.out.println("Avez vous un bagage ? (0 pour non, 1 pour oui) :");
                int testif = sc.nextInt();
                sc.nextLine();

                if (testif == 1) {
                    v.setBagage(a);
                    System.out.println("Saisissez le numéro de votre bagage : ");

                    int numero = sc.nextInt();
                    a.setNumero(numero);
                    sc.nextLine();

                    System.out.println("De quelle couleur est votre bagage ?");
                    String couleur = sc.nextLine();
                    a.setCouleur(couleur);

                    System.out.println("Combien pèse votre bagage ?");
                    double poids = sc.nextDouble();
                    a.setPoids(poids);
                    sc.nextLine();
                } else if (testif == 0) {
                    System.out.println(v.toString());
                } else if(testspe==1){
                    VoyageurPrivilege vp = new VoyageurPrivilege();
                    vp.setCode(codep);
                    vp.setAge(age);
                    vp.setNom(nom);
                    vp.setAdresse(x);
                    vp.setBagage(a);
                    agv.addVoyageur(vp);
                } else if (testhandi==1){
                    VoyageurHandicape vh = new VoyageurHandicape();
                    vh.setHandicap(handi);
                    vh.setAge(age);
                    vh.setNom(nom);
                    vh.setAdresse(x);
                    vh.setBagage(a);
                    agv.addVoyageur(vh);
                }

                agv.addVoyageur(v);
            }
            if (appli == 2) {
                System.out.println("Entrez le nom de ce voyageur : ");
                String search = sc.nextLine();
                agv.search(search);
            }
            if (appli == 3) {
                System.out.println("Entrez le nom de ce voyageur : ");
                String suppr = sc.nextLine();
                agv.del(suppr);
            }
            if (appli == 4) {
                System.out.println(agv.toString());
            }
        }
    }
}