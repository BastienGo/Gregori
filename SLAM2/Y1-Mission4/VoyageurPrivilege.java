package eu.siohautil.heritage;

import eu.siohautil.base.Voyageur;

/** Classe VoyageurPrivilege
 * @author BastienGre
 * @version 1.0
 */

public class VoyageurPrivilege extends Voyageur {
    /**
     * code est un entier (ex : 10)
     */
    private int code;
    /**
     * Un constructeur vide
     */
    public VoyageurPrivilege(){

    }

    /**
     * Un constructeur à 3 paramètres, nom, age et code
     * @param nom
     * @param age
     * @param code
     */
    public VoyageurPrivilege(String nom, int age, int code){
        super(nom, age);
        this.code = code;
    }

    /**
     * Une méthode qui récupère le code
     * @return le code
     */
    public int getCode() {
        return code;
    }

    /**
     * Une méthode qui permet de changer le code
     * @param code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * Une méthode qui permet d'afficher le code
     * @return
     */
    @Override
    public String toString() {
        return super.toString() + " " + this.code;
    }
}
