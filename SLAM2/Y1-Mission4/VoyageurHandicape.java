package eu.siohautil.heritage;

import eu.siohautil.base.Voyageur;

/** Classe VoyageurHandicape
 * @author BastienGre
 * @version 1.0
 */

public class VoyageurHandicape extends Voyageur{
    /**
     * handicap est une chaine de caractère (ex : Fauteuil roulant)
     */
    private String handicap;

    /**
     * Un constructeur vide
     */
    public VoyageurHandicape(){

    }

    /**
     * Un constructeur à 3 paramètres, nom, age et handicap
     * @param nom
     * @param age
     * @param handicap
     */
    public VoyageurHandicape(String nom, int age, String handicap){
        super(nom, age);
        this.handicap = handicap;
    }

    /**
     * Une méthode qui permet de récupérer la chaine handicap
     * @return la chaine handicap
     */
    public String getHandicap() {
        return handicap;
    }

    /**
     * Une méthode qui permet de modifier handicap
     * @param handicap
     */
    public void setHandicap(String handicap) {
        this.handicap = handicap;
    }

    /**
     * Une méthode qui permet l'affichage de handicap
     * @return
     */
    @Override
    public String toString() {
        return super.toString() + " " + this.handicap;
    }
}