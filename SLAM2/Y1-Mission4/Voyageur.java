package eu.siohautil.base;

/** Classe Voyageur
 * @author BastienGre
 * @version 1.0
 */

public class Voyageur {
    /**
     * nom est une chaine de caractères (ex : Bastien)
     */
    protected String nom;
    /**
     * age est un entier (ex : 20)
     */
    protected int age;
    /**
     * categorie est une chaine de caractères (ex : nourisson)
     */
    protected String categorie;
    /**
     * adresse est une AdressePostale (ex : 9 rue de l'enfance 95490 Vaureal)
     */
    protected AdressePostale adresse;
    /**
     * bagage est un Bagage (ex : 1 Rouge 20,5)
     */
    protected Bagage bagage;

    /**
     * Une méthode qui permet de changer nom
     *
     * @param nom
     */
    public void setNom(String nom) {
        if (nom.length() >= 2) {
            this.nom = nom;
        }
    }

    /**
     * Une méthode qui récupère le nom
     *
     * @return la chaine de caractère nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Une méthode qui permet de changer age
     *
     * @param age
     */
    public void setAge(int age) {
        if (age > 0) {
            this.age = age;
            setCategorie();
        }
    }

    /**
     * Une méthode qui récupère l'age
     *
     * @return l'entier age
     */
    public int getAge() {
        return this.age;
    }

    /**
     * Une méthode qui permet de changer categorie
     */
    protected void setCategorie() {
        if (age < 2) {
            this.categorie = "nourisson";
        } else if (age < 10) {
            this.categorie = "enfant";
        } else if (age < 18) {
            this.categorie = "adolescent";
        } else if (age < 55) {
            this.categorie = "adulte";
        } else {
            this.categorie = "senior";
        }
    }

    /**
     * Une méthode qui récupère categorie
     *
     * @return la chaine de caractere catégorie
     */
    public String getCategorie() {
        return this.categorie;
    }

    /**
     * Un constructeur vide
     */
    public Voyageur() {
        nom = " ";
        age = 0;
        setCategorie();
    }

    /**
     * Un constructeur à deux paramètres, nom et age
     *
     * @param nom
     * @param age
     */
    public Voyageur(String nom, int age) {
        this.nom = nom;
        this.age = age;
        setCategorie();
    }

    /**
     * Une méthode qui permet de changer adresse
     *
     * @param adresse
     */
    public void setAdresse(AdressePostale adresse) {
        this.adresse = adresse;
    }

    /**
     * Une méthode qui récupère adresse
     *
     * @return l'AdressePostale adresse
     */
    public AdressePostale getAdresse() {
        return adresse;
    }

    /**
     * Une méthode qui permet de changer bagage
     *
     * @param bagage
     */
    public void setBagage(Bagage bagage) {
        this.bagage = bagage;
    }

    /**
     * Une méthode qui récupère bagage
     *
     * @return le Bagage bagage
     */
    public Bagage getBagage() {
        return bagage;
    }

    /**
     * Une méthode qui affiche le descriptif du voyageur, et son adresse si elle est renseignée
     */
    @Override
    public String toString() {
        String verad;
        String verbag;
        if(this.adresse != null){
            verad = this.adresse.toString();
        }
        else{
            verad = "Adresse non renseignée.";
        }
        if (this.bagage != null) {
            verbag = this.bagage.toString();
        } else {
            verbag = "Bagage non renseigné.";
        }
        return ("Descriptif du voyageur :(" + nom + ", " + age + ", " + categorie + ") " + verad + " " + verbag);
        }
    }