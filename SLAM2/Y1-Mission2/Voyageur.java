class Voyageur{
    private String nom;
    private int age;
    private String categorie;
    private AdressePostale adresse;
    private Bagage bagage;

    public void setNom(String nom){
        if (nom.length()>= 2){
            this.nom=nom;
        }
    }

    public String getNom(){
        return this.nom;
    }

    public void setAge(int age){
        if (age > 0){
            this.age=age;
            setCategorie();
        }
    }

    public int getAge(){
        return this.age;
    }

    private void setCategorie(){
        if (age < 2){
            this.categorie="nourisson";
        }
        else if (age >=2 && age <10){
            this.categorie="enfant";
        }
        else if (age >=10 && age<18){
            this. categorie="adolescent";
        }
        else if (age >=18 && age<55){
            this.categorie="adulte";
        }
        else{
            this.categorie="senior";
        }
    }

    public String getCategorie(){
        return this.categorie;
    }

    public Voyageur(){
        nom = " ";
        age = 0;
        setCategorie();
    }

    public Voyageur(String nom, int age){
        this.nom=nom;
        this.age=age;
        setCategorie();
    }

    public void setAdresse(AdressePostale adresse){
        this.adresse=adresse;
    }

    public AdressePostale getAdresse() {
        return adresse;
    }

    public void setBagage(Bagage bagage){
        this.bagage=bagage;
    }

    public Bagage getBagage(){
        return bagage;
    }

    void afficher () {
        System.out.println ("Descriptif du voyageur :("+nom+","+age+","+categorie+")");
        if(this.adresse != null){
            this.adresse.afficher();
        }
        else{
            System.out.println("Adresse non renseignée.");
        }
        if(this.bagage != null{
            this.bagage.afficher();
        }
        {
            System.out.println("Bagage non renseigné.");
        }
    }
    
    void afficher2(){
        System.out.println("Descriptif du voyageur : ("+nom+", "+age+", "+categorie+")");
        if(this.adresse != null){
            this.adresse.afficher();
        }
        else{
            System.out.println("Adresse non renseignée.");
        }
        System.out.println("Pas de bagage pour ce voyageur.");
    }
}
