import java.util.Scanner;
public class Mission2_E2 {
    private static Scanner sc;

    public static void main(String arg[]) {
        sc = new Scanner(System.in);

        AdressePostale nv= new AdressePostale("9 rue de l'enfance","Vaureal","95490");

        Voyageur v= new Voyageur("Bastien",20);
        v.setAdresse(nv);
        v.afficher();

        System.out.println("Quelle est votre adresse ?");
        String adresse = sc.nextLine();
        nv.setAdresse(adresse);

        System.out.println("Quelle est votre ville ?");
        String ville = sc.nextLine();
        nv.setVille(ville);

        System.out.println("Quel est votre code postal ?");
        String CP= sc.nextLine();
        nv.setCP(CP);

        v.setAdresse(nv);
        v.afficher();
    }
}