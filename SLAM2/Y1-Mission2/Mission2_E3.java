import java.util.Scanner;
public class Mission2_E3 {

    private static Scanner sc;
    public static void main (String arg []) {
        sc = new Scanner(System.in);

        Bagage nvb = new Bagage(1,"Rouge",19);
        nvb.afficher();

        System.out.println("Saisissez le numéro du bagage :");
        int numero = sc.nextInt();
        nvb.setNumero(numero);

        System.out.println("Saisissez la couleur du bagage :");
        String couleur = sc.next();
        nvb.setCouleur(couleur);

        System.out.println("Saisissez le poids du bagage :");
        double poids = sc.nextDouble();
        nvb.setPoids(poids);

        nvb.afficher();
    }
}
