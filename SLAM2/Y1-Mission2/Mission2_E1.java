import java.util.Scanner;
public class Mission2_E1{
    private static Scanner sc;
    public static void main (String arg []){
        sc = new Scanner(System.in);

        AdressePostale v = new AdressePostale("9 rue de l'enfance","Vaureal", "95490");
        v.afficher();

        System.out.println("Quelle est votre adresse ? (Rue et N°)");
        String adresse=sc.nextLine();
        v.setAdresse(adresse);

        System.out.println("Quelle est votre ville ?");
        String ville=sc.nextLine();
        v.setVille(ville);

        System.out.println("Quel est votre code postal ?");
        String CP=sc.nextLine();
        v.setCP(CP);

        v.afficher();
    }
}
