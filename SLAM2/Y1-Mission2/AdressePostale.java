public class AdressePostale {
    private String adresse;
    private String ville;
    private String cp;

    public AdressePostale(String adresse, String ville, String CP){
        this.adresse=adresse;
        this.ville=ville;
        this.cp=cp;
    }

    public void setAdresse(String adresse){
        this.adresse=adresse;
    }

    public String getAdresse(){return this.adresse;}

    public void setVille(String ville){
        this.ville=ville;
    }

    public String getVille(){return this.ville;}

    public void setCP(String cp){
        this.cp=cp;
    }

    public String getCP(){return this.cp;}

    void afficher () {System.out.println("Adresse complète : "+adresse+" "+ville+" "+cp);}
}
