import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JDBCSimple {
    public static void main(String[] args) {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("driver ok");
            Connection conn=
                    DriverManager.getConnection("jdbc:mysql://sl-eu-lon-2-portal.0.dblayer.com:20978/BTS","admin",
                            "KOFAAADAFEPBCEGD");
            System.out.println("connection ok");

            Statement stmt = conn.createStatement();

            String requestin = "INSERT INTO professeurs VALUES (32,'Abdelmoula','SLAM')"; // On prépare l'insertion des données au préalable
            int res = stmt.executeUpdate(requestin);// On execute l'insertion des données, c'est compté comme une mise à jour
            System.out.println("nb de modifications réalisées : " + res);

            String requestsel = "SELECT nom, specialite FROM professeurs";
            ResultSet resultsel = stmt.executeQuery(requestsel);
            while(resultsel.next()){
                System.out.println("Nom : " + resultsel.getString(1));
                System.out.println("Spécialité : " + resultsel.getString(2));
            }

            resultsel.close();
            stmt.close();

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}