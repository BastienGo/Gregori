import java.sql.*;
import java.util.Scanner;

public class JDBCPrepare {
    private static Scanner sc;
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("driver ok");
            Connection conn=
                    DriverManager.getConnection("jdbc:mysql://sl-eu-lon-2-portal.0.dblayer.com:20978/BTS","admin",
                            "KOFAAADAFEPBCEGD");
            System.out.println("connection ok");

            System.out.println("1 pour insérer, 2 pour rechercher une spé, 3 pour supprimer, 4 pour rechercher un nom, 0 pour quitter : ");
            int choix = sc.nextInt();

            Statement stmt = conn.createStatement();

            while(choix<0 || choix > 4){
                System.out.println("Ce chiffre n'est pas attribué, recommencez : ");
                choix = sc.nextInt();
            }
            if(choix == 1){
                String requestin = "INSERT INTO professeurs VALUES(?,?,?)";
                PreparedStatement pstmt = conn.prepareStatement(requestin);

                System.out.println("Saisissez l'id du professeur : ");
                int id = sc.nextInt();

                sc.nextLine();

                System.out.println("Saisissez le nom du professeur : ");
                String nom = sc.nextLine();

                System.out.println("Saisissez la spécialité du professeur : ");
                String specialite = sc.nextLine();

                pstmt.setInt(1,id);
                pstmt.setString(2,nom);
                pstmt.setString(3,specialite);

                int res2 = pstmt.executeUpdate();

                System.out.println("nb de modifications réalisées : " + res2);
            }
            if(choix == 2){

                System.out.println("Saisissez la spécialité recherchée : ");
                String sperech = sc.nextLine();

                String requestsel = "SELECT nom, specialite FROM professeurs WHERE specialite = ?";
                PreparedStatement pstmt2 = conn.prepareStatement(requestsel);
                pstmt2.setString(1,sperech);

                ResultSet resultsel = pstmt2.executeQuery();
                while(resultsel.next()){
                    System.out.println("Nom : " + resultsel.getString(1));
                    System.out.println("Spécialité : " + resultsel.getString(2));
                }
                resultsel.close();
            }
            if(choix == 3){
                System.out.println("Saisissez l'id du professeur à supprimer de la base : ");
                int profsup = sc.nextInt();

                String requestdel = "DELETE FROM professeurs WHERE id= ?";
                PreparedStatement pstmt3 = conn.prepareStatement(requestdel);
                pstmt3.setInt(1,profsup);

                int res3 = pstmt3.executeUpdate();

                System.out.println("nb de modifications réalisées : " + res3);

            }
            if(choix == 4){
                System.out.println("Saisissez le nom recherché : ");
                String nomrech = sc.next();

                String requestsel = "SELECT nom, specialite FROM professeurs WHERE nom = ?";
                PreparedStatement pstmt4 = conn.prepareStatement(requestsel);
                pstmt4.setString(1,nomrech);

                ResultSet resultsel2 = pstmt4.executeQuery();
                while(resultsel2.next()){
                    System.out.println("Nom : " + resultsel2.getString(1));
                    System.out.println("Spécialité : " + resultsel2.getString(2));
                }
                resultsel2.close();
            }
            stmt.close();

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}