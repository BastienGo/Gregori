import java.util.Scanner;
class Mission1 {
    private static Scanner sc;
    public static void main( String arg [ ] ) {
        sc = new Scanner(System.in);

        Voyageur [] tabloVoyageur= new Voyageur[5];
        tabloVoyageur[0]=new Voyageur("Eric", 50);
        tabloVoyageur[1]=new Voyageur("Nathalie", 51);
        tabloVoyageur[2]=new Voyageur("Bastien", 20);
        tabloVoyageur[3]=new Voyageur("Aurélie", 29);
        tabloVoyageur[4]=new Voyageur("Michael", 34);
        Voyageur v = new Voyageur();
        Voyageur nv = new Voyageur ();
        System.out.println("Quel est votre nom ? ");
        String nom = sc.next();
        nv.setNom(nom);
        System.out.println("Quel est votre âge ? ");
        int age = sc.nextInt();
        nv.setAge(age);
        nv.afficher();
        v.afficher();
        for(int i=0; i<5; i++){
            tabloVoyageur[i].afficher();
        }
    }
}