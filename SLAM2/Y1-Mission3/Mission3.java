package eu.siohautil.test;

import eu.siohautil.mission3.AgenceVoyage;
import eu.siohautil.mission3.AdressePostale;
import eu.siohautil.mission3.Bagage;
import eu.siohautil.mission3.Voyageur;
import java.util.ArrayList;
import java.util.Scanner;

public class Mission3 {
    private static Scanner sc;

    public static void main (String []  args) {
        sc = new Scanner(System.in);
        ArrayList<Voyageur> voys = new ArrayList<Voyageur>();
        AgenceVoyage agv = new AgenceVoyage();
        System.out.println("Quel est le nom de votre agence de voyage ?");
        String nomav = sc.nextLine();

        System.out.println("Quelle est son adresse ? (N° et nom de rue)");
        String numrueav = sc.nextLine();
        System.out.println("Quelle est la ville ou elle se situe ?");
        String villeav = sc.nextLine();
        System.out.println("Quel est le code postal de la ville ?");
        String cpav = sc.nextLine();

        AdressePostale adresseav = new AdressePostale();
        adresseav.setAdresse(numrueav);
        adresseav.setCP(cpav);
        adresseav.setVille(villeav);

        agv.setAdresse(adresseav);
        agv.setNom(nomav);

        System.out.println("Bienvenue, que souhaitez vous faire ? 1 permet d'ajouter un voyageur, 2 permet de rechercher un voyageur, 3 permet de supprimer un voyageur, 4 affiche l'agence et la liste de voyageurs, et 5 quitte l'application.");
        int appli = sc.nextInt();

        while (appli != 5){
            
        if (appli == 1) {
            AdressePostale x = new AdressePostale();
            Bagage a = new Bagage();
            Voyageur v = new Voyageur();

            System.out.println("Saisissez votre nom: ");
            String nom = sc.next();
            v.setNom(nom);

            System.out.println("Quel est votre âge ?");
            int age = sc.nextInt();
            v.setAge(age);
            sc.nextLine();

            System.out.println("Saisissez votre adresse: ");
            String adresse = sc.nextLine();
            x.setAdresse(adresse);

            System.out.println("Saisissez votre ville: ");
            String ville = sc.nextLine();
            x.setVille(ville);

            System.out.println("Saisissez votre code postal: ");
            String codePostale = sc.nextLine();
            x.setCP(codePostale);

            v.setAdresse(x);

            System.out.println("Avez vous un bagage ? (0 pour non, 1 pour oui) :");
            int testif = sc.nextInt();
            sc.nextLine();

            if (testif == 1) {
                v.setBagage(a);
                System.out.println("Saisissez le numéro de votre bagage : ");

                int numero = sc.nextInt();
                a.setNumero(numero);
                sc.nextLine();

                System.out.println("De quelle couleur est votre bagage ?");
                String couleur = sc.nextLine();
                a.setCouleur(couleur);

                System.out.println("Combien pèse votre bagage ?");
                double poids = sc.nextDouble();
                a.setPoids(poids);
                sc.nextLine();
            } else if (testif == 0) {
                v.afficher();
            }

            agv.addVoyageur(v);
        }

        if (appli == 2){
            System.out.println("Entrez le nom de ce voyageur à rechercher dans la liste : ");
            String search = sc.nextLine();
            agv.search(search);
        }

        if (appli == 3){
            System.out.println("Entrez le nom du voyageur à supprimer de la liste : ");
            String suppr = sc.nextLine();
            agv.del(suppr);
        }

        if (appli == 4) {
            agv.afficher();
        }
        }
    }
}